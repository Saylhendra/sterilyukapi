<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @property Jadwal_model $Jadwal_model
 * @property Kru_model $Kru_model
 * @property Book_model $Book_model
 * @property Lokasi_jadwal_model $Lokasi_jadwal_model
 * @property To_Book_model $To_Book_model
 * @property Jadwal_block_user_model $Jadwal_block_user_model
 * @property User_model $User_model
 * @property Status_aktif_model $Status_aktif_model
 * @property Status_jadwal_model $Status_jadwal_model
 */


class Jadwal_versi_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Jadwal_model', '', TRUE);
        $this->load->model('Kru_model', '', TRUE);
        $this->load->model('Book_model', '', TRUE);
        $this->load->model('Lokasi_jadwal_model', '', TRUE);
        $this->load->model('To_Book_model', '', TRUE);
        $this->load->model('Jadwal_block_user_model', '', TRUE);
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Status_aktif_model', '', TRUE);
        $this->load->model('Status_jadwal_model', '', TRUE);

    }

    public function index()
    {
        echo "Access Denied";
    }


    function buatrp($angka)
    {
        $jadi = "Rp " . number_format($angka, 0, ',', '.');
        return $jadi;
    }

    function formatIndonesia($waktu_mulai_jadwal)
    {
        $w = explode(" ", $waktu_mulai_jadwal);
        $x = explode("-", $w[0]);
        $bulan = "";
        if ($x[1] == "01") {
            $bulan = "Januari";
        }
        if ($x[1] == "02") {
            $bulan = "Februari";
        }
        if ($x[1] == "03") {
            $bulan = "Maret";
        }
        if ($x[1] == "04") {
            $bulan = "April";
        }
        if ($x[1] == "05") {
            $bulan = "Mei";
        }
        if ($x[1] == "06") {
            $bulan = "Juni";
        }
        if ($x[1] == "07") {
            $bulan = "Juli";
        }
        if ($x[1] == "08") {
            $bulan = "Agustus";
        }
        if ($x[1] == "09") {
            $bulan = "September";
        }
        if ($x[1] == "10") {
            $bulan = "Oktober";
        }
        if ($x[1] == "11") {
            $bulan = "November";
        }
        if ($x[1] == "12") {
            $bulan = "Desember";
        }

        return $x[2] . " " . $bulan . " " . $x[0];

    }

    function jadwal($kode_tipe_jadwal, $page = null, $id_user = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['jadwal'] = $this->Jadwal_model->get_jadwal($kode_tipe_jadwal, $page, $id_user);
        echo json_encode($response);
    }

    function my_book($page = null, $id_user = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['jadwal'] = $this->Jadwal_model->my_book($page, $id_user);
        echo json_encode($response);
    }

    function my_record_book($page = null, $id_user = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['jadwal'] = $this->Jadwal_model->my_record_book($page, $id_user);
        echo json_encode($response);
    }

    function count_my_book($id_user = null)
    {
        $response['count_my_book'] = $this->Jadwal_model->count_my_book($id_user);
        echo json_encode($response);
    }

    function to_book($page = null, $id_user = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['jadwal'] = $this->Jadwal_model->to_book($page, $id_user);
        echo json_encode($response);
    }


    function detail_jadwal($id_jadwal, $id_user = null, $id_level_user = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_user);

        if ($detail_jadwal == null) {
            $response['isSuccess'] = false;
            $response['message'] = "not available";
        } else {

            $kru_jadwal = $this->Kru_model->get_kru($id_jadwal, 1, 3);
            $book_jadwal = $this->Book_model->get_book($id_level_user, $id_jadwal, 1, 3);
            $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
            $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
            $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
            $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
            $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
            $response['biaya_steril_jantan_lokal'] = $detail_jadwal["biaya_steril_jantan_lokal"];
            $response['biaya_steril_jantan_mix'] = $detail_jadwal["biaya_steril_jantan_mix"];
            $response['biaya_steril_betina_lokal'] = $detail_jadwal["biaya_steril_betina_lokal"];
            $response['biaya_steril_betina_mix'] = $detail_jadwal["biaya_steril_betina_mix"];
            $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
            $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
            $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
            $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
            $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
            $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
            $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
            $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
            $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
            $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
            $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
            $response['nama_tipe_jadwal'] = $detail_jadwal["nama_tipe_jadwal"];
            $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
            $response['alamat_lokasi_jadwal'] = $detail_jadwal["alamat_lokasi_jadwal"];
            $response['rute_lokasi_jadwal'] = $detail_jadwal["rute_lokasi_jadwal"];
            $response['no_telp_lokasi_jadwal'] = $detail_jadwal["no_telp_lokasi_jadwal"];
            $response['latitude_lokasi_jadwal'] = $detail_jadwal["latitude_lokasi_jadwal"];
            $response['longitude_lokasi_jadwal'] = $detail_jadwal["longitude_lokasi_jadwal"];
            $response['id_status_jadwal'] = $detail_jadwal["id_status_jadwal"];
            $response['nama_status_jadwal'] = $detail_jadwal["nama_status_jadwal"];
            $response['id_user'] = $detail_jadwal["id_user"];
            $response['id_book'] = $detail_jadwal["id_book"];
            $response['kode_book'] = $detail_jadwal["kode_book"];
            $response['id_status_book'] = $detail_jadwal["id_status_book"];
            $response['biaya_book'] = $detail_jadwal["biaya_book"];
            $response['kuota_jantan_book'] = $detail_jadwal["kuota_jantan_book"];
            $response['kuota_jantan_lokal_book'] = $detail_jadwal["kuota_jantan_lokal_book"];
            $response['kuota_jantan_mix_book'] = $detail_jadwal["kuota_jantan_mix_book"];
            $response['kuota_betina_book'] = $detail_jadwal["kuota_betina_book"];
            $response['kuota_betina_lokal_book'] = $detail_jadwal["kuota_betina_lokal_book"];
            $response['kuota_betina_mix_book'] = $detail_jadwal["kuota_betina_mix_book"];
            $response['waktu_book'] = $detail_jadwal["waktu_book"];
            $response['nama_status_book'] = $detail_jadwal["nama_status_book"];
            $response['id_pembayaran_pendaftaran_peserta'] = $detail_jadwal["id_pembayaran_pendaftaran_peserta"];
            $response['id_bank_tujuan_pembayaran'] = $detail_jadwal["id_bank_tujuan_pembayaran"];
            $response['bank_yang_digunakan'] = $detail_jadwal["bank_yang_digunakan"];
            $response['nomor_rekening_bank_yang_digunakan'] = $detail_jadwal["nomor_rekening_bank_yang_digunakan"];
            $response['nama_pemilik_nomor_rekening_bank_yang_digunakan'] = $detail_jadwal["nama_pemilik_nomor_rekening_bank_yang_digunakan"];
            $response['tanggal_transfer'] = $detail_jadwal["tanggal_transfer"];
            $response['keterangan'] = $detail_jadwal["keterangan"];
            $response['waktu_pembayaran_pendaftaran_peserta'] = $detail_jadwal["waktu_pembayaran_pendaftaran_peserta"];
            $response['foto_bukti_pembayaran'] = $detail_jadwal["foto_bukti_pembayaran"];
            $response['id_status_pembayaran_pendaftaran_peserta'] = $detail_jadwal["id_status_pembayaran_pendaftaran_peserta"];
            $response['nama_status_pembayaran_pendaftaran_peserta'] = $detail_jadwal["nama_status_pembayaran_pendaftaran_peserta"];
            $response['nama_bank_tujuan_pembayaran'] = $detail_jadwal["nama_status_book"];
            $response['nomor_rekening_bank_tujuan_pembayaran'] = $detail_jadwal["nomor_rekening_bank_tujuan_pembayaran"];
            $response['atas_nama_bank_tujuan_pembayaran'] = $detail_jadwal["atas_nama_bank_tujuan_pembayaran"];
            $response['isToBook'] = $this->To_Book_model->check_exitst_by_id_jadwal_id_user($id_jadwal, $id_user);
            $response['is_blocked_user'] = $this->Jadwal_block_user_model->check_exitst_by_id_jadwal_id_user($id_jadwal, $id_user);
            $response['kru'] = $kru_jadwal;
            $response['book'] = $book_jadwal;
        }
        echo json_encode($response);
    }

    function book()
    {
        $limit = $this->input->post('limit');
        $status = $this->input->post('status');
        $kode_jadwal = $this->input->post('kode_jadwal');
        $id_book_terbaru = $this->input->post('id_book_terbaru');
        $id_book_terlama = $this->input->post('id_book_terlama');

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($kode_jadwal != null) {
            $response['isSuccess'] = true;
            $response['message'] = "berhasil";
            $response['book'] = $this->Book_model->get_book($status, $kode_jadwal, $id_book_terbaru, $id_book_terlama, $limit);
        }
        echo json_encode($response);
    }

    function prepare_add_jadwal()
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['lokasi_jadwal'] = $this->Lokasi_jadwal_model->get_all_lokasi_jadwal();
        $response['kru'] = $this->User_model->get_all_tim();
        $response['status_aktif'] = $this->Status_aktif_model->get_all_status_aktif();
        $response['status_jadwal'] = $this->Status_jadwal_model->get_all_status_jadwal();
        echo json_encode($response);
    }

    function prepare_update_jadwal($id_jadwal)
    {
        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($id_jadwal != null) {
            $id_level_user = null;
            $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_level_user);
            if ($detail_jadwal == null) {
                $response['isSuccess'] = false;
                $response['message'] = "not available";
            } else {
                $response['isSuccess'] = true;
                $response['message'] = "berhasil";

                $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
                $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
                $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
                $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
                $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
                $response['biaya_steril_jantan_lokal'] = $detail_jadwal["biaya_steril_jantan_lokal"];
                $response['biaya_steril_jantan_mix'] = $detail_jadwal["biaya_steril_jantan_mix"];
                $response['biaya_steril_betina_lokal'] = $detail_jadwal["biaya_steril_betina_lokal"];
                $response['biaya_steril_betina_mix'] = $detail_jadwal["biaya_steril_betina_mix"];
                $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
                $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
                $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
                $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
                $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
                $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
                $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
                $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
                $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
                $response['id_status_jadwal'] = $detail_jadwal["id_status_jadwal"];
                $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
                $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
                $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
                $response['nama_status_jadwal'] = $detail_jadwal["nama_status_jadwal"];
                $response['kru_jadwal'] = $this->Kru_model->get_all_kru_byIdJadwal($id_jadwal);

                $response['lokasi_jadwal'] = $this->Lokasi_jadwal_model->get_all_lokasi_jadwal();
                $response['kru'] = $this->User_model->get_all_tim();
                $response['status_aktif'] = $this->Status_aktif_model->get_all_status_aktif();
                $response['status_jadwal'] = $this->Status_jadwal_model->get_all_status_jadwal();
            }
        } else {
            $response['message'] = "Tidak ada jadwal yang terpilih";
        }
        echo json_encode($response);
    }

    function addjadwal()
    {

        $kd = $this->Jadwal_model->getNewKodeJadwal();
        $kode_jadwal = "#J" . ((int)str_replace("#J", "", $kd["kode_jadwal"]) + 1);
        $waktu_mulai_jadwal = $this->input->post('waktu_mulai_jadwal');
        $waktu_akhir_jadwal = $this->input->post('waktu_akhir_jadwal');
        $keterangan_jadwal = $this->input->post('keterangan_jadwal');
        $biaya_steril_jantan_lokal = $this->input->post('biaya_steril_jantan_lokal');
        $biaya_steril_jantan_mix = $this->input->post('biaya_steril_jantan_mix');
        $biaya_steril_betina_lokal = $this->input->post('biaya_steril_betina_lokal');
        $biaya_steril_betina_mix = $this->input->post('biaya_steril_betina_mix');
        $kuota_jantan = $this->input->post('kuota_jantan');
        $kuota_betina = $this->input->post('kuota_betina');
        $sisa_kuota_jantan = $this->input->post('sisa_kuota_jantan');
        $sisa_kuota_betina = $this->input->post('sisa_kuota_betina');
        $id_tipe_jadwal = $this->input->post('id_tipe_jadwal');
        $id_lokasi_jadwal = $this->input->post('id_lokasi_jadwal');
        $id_status_aktif = $this->input->post('id_status_aktif');
        $id_status_jadwal = $this->input->post('id_status_jadwal');

        $id_user = $this->input->post('id_user');

        $array_waktu_mulai_jadwal = explode('-', $waktu_mulai_jadwal);
        $waktu_mulai_jadwal = $array_waktu_mulai_jadwal[2] . "-" . $array_waktu_mulai_jadwal[1] . "-" . $array_waktu_mulai_jadwal[0] . " 00:00:00"; // echos 'Order'

        if ($biaya_steril_jantan_lokal == null) {
            $biaya_steril_jantan_lokal = "0";
        }
        if ($biaya_steril_jantan_mix == null) {
            $biaya_steril_jantan_mix = "0";
        }
        if ($biaya_steril_betina_lokal == null) {
            $biaya_steril_betina_lokal = "0";
        }
        if ($biaya_steril_betina_mix == null) {
            $biaya_steril_betina_mix = "0";
        }

        if ($waktu_akhir_jadwal == null) {
            $waktu_akhir_jadwal = "0";
        } else {
            $array_waktu_akhir_jadwal = explode('-', $waktu_akhir_jadwal);
            $waktu_akhir_jadwal = $array_waktu_akhir_jadwal[2] . "-" . $array_waktu_akhir_jadwal[1] . "-" . $array_waktu_akhir_jadwal[0] . " 00:00:00"; // echos 'Order'
        }

        if ($keterangan_jadwal == null) {
            $keterangan_jadwal = " - ";
        }

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($waktu_mulai_jadwal != null || $kuota_jantan != null || $kuota_betina != null || $id_tipe_jadwal != null || $id_lokasi_jadwal != null) {
            $jadwal = array(
                'kode_jadwal' => $kode_jadwal,
                'waktu_mulai_jadwal' => $waktu_mulai_jadwal,
                'waktu_akhir_jadwal' => $waktu_akhir_jadwal,
                'keterangan_jadwal' => $keterangan_jadwal,
                'biaya_steril_jantan_lokal' => $biaya_steril_jantan_lokal,
                'biaya_steril_jantan_mix' => $biaya_steril_jantan_mix,
                'biaya_steril_betina_lokal' => $biaya_steril_betina_lokal,
                'biaya_steril_betina_mix' => $biaya_steril_betina_mix,
                'kuota_jantan' => $kuota_jantan,
                'kuota_betina' => $kuota_betina,
                'sisa_kuota_jantan' => $sisa_kuota_jantan,
                'sisa_kuota_betina' => $sisa_kuota_betina,
                'id_tipe_jadwal' => $id_tipe_jadwal,
                'id_lokasi_jadwal' => $id_lokasi_jadwal,
                'id_status_aktif' => $id_status_aktif,
                'id_status_jadwal' => $id_status_jadwal

            );

            $action_jadwal = $this->Jadwal_model->insertjadwal($jadwal);

            if ($action_jadwal) {
                if ($id_user != null) {
                    $array_id_user = explode('|', $id_user);
                    $tr = $this->Jadwal_model->getJadwalByKodeJadwal($kode_jadwal);
                    for ($x = 0; $x < count($array_id_user); $x++) {
                        $kru = array(
                            'id_jadwal' => $tr["id_jadwal"],
                            'id_user' => $array_id_user[$x],
                            'id_tipe_kru' => "1",
                        );
                        if ($array_id_user[$x] != "") {
                            $this->Kru_model->insertkru($kru);
                        }
                    }
                }
                $response['isSuccess'] = true;
                $response['message'] = "berhasil menambah jadwal";
            } else {
                $response['message'] = "gagal menambah jadwal";
            }
        }
        echo json_encode($response);
    }

    function updatejadwal()
    {
        $id_jadwal = $this->input->post('id_jadwal');
        $waktu_mulai_jadwal = $this->input->post('waktu_mulai_jadwal');
        $waktu_akhir_jadwal = $this->input->post('waktu_akhir_jadwal');
        $keterangan_jadwal = $this->input->post('keterangan_jadwal');
        $biaya_steril_jantan_lokal = $this->input->post('biaya_steril_jantan_lokal');
        $biaya_steril_jantan_mix = $this->input->post('biaya_steril_jantan_mix');
        $biaya_steril_betina_lokal = $this->input->post('biaya_steril_betina_lokal');
        $biaya_steril_betina_mix = $this->input->post('biaya_steril_betina_mix');
        $kuota_jantan = $this->input->post('kuota_jantan');
        $kuota_betina = $this->input->post('kuota_betina');
        $sisa_kuota_jantan = $this->input->post('sisa_kuota_jantan');
        $sisa_kuota_betina = $this->input->post('sisa_kuota_betina');
        $id_tipe_jadwal = $this->input->post('id_tipe_jadwal');
        $id_lokasi_jadwal = $this->input->post('id_lokasi_jadwal');
        $id_status_aktif = $this->input->post('id_status_aktif');
        $id_status_jadwal = $this->input->post('id_status_jadwal');

        $id_user = $this->input->post('id_user');

        $array_waktu_mulai_jadwal = explode('-', $waktu_mulai_jadwal);
        $waktu_mulai_jadwal = $array_waktu_mulai_jadwal[2] . "-" . $array_waktu_mulai_jadwal[1] . "-" . $array_waktu_mulai_jadwal[0] . " 00:00:00"; // echos 'Order'

        if ($biaya_steril_jantan_lokal == null) {
            $biaya_steril_jantan_lokal = "0";
        }
        if ($biaya_steril_jantan_mix == null) {
            $biaya_steril_jantan_mix = "0";
        }
        if ($biaya_steril_betina_lokal == null) {
            $biaya_steril_betina_lokal = "0";
        }
        if ($biaya_steril_betina_mix == null) {
            $biaya_steril_betina_mix = "0";
        }

        if ($waktu_akhir_jadwal == null) {
            $waktu_akhir_jadwal = "0";
        } else {
            $array_waktu_akhir_jadwal = explode('-', $waktu_akhir_jadwal);
            $waktu_akhir_jadwal = $array_waktu_akhir_jadwal[2] . "-" . $array_waktu_akhir_jadwal[1] . "-" . $array_waktu_akhir_jadwal[0] . " 00:00:00"; // echos 'Order'
        }

        if ($keterangan_jadwal == null) {
            $keterangan_jadwal = " - ";
        }

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($id_jadwal != null && $waktu_mulai_jadwal != null || $kuota_jantan != null || $kuota_betina != null || $id_tipe_jadwal != null || $id_lokasi_jadwal != null) {
            $jadwal = array(
                'waktu_mulai_jadwal' => $waktu_mulai_jadwal,
                'waktu_akhir_jadwal' => $waktu_akhir_jadwal,
                'keterangan_jadwal' => $keterangan_jadwal,
                'biaya_steril_jantan_lokal' => $biaya_steril_jantan_lokal,
                'biaya_steril_jantan_mix' => $biaya_steril_jantan_mix,
                'biaya_steril_betina_lokal' => $biaya_steril_betina_lokal,
                'biaya_steril_betina_mix' => $biaya_steril_betina_mix,
                'kuota_jantan' => $kuota_jantan,
                'kuota_betina' => $kuota_betina,
                'sisa_kuota_jantan' => $sisa_kuota_jantan,
                'sisa_kuota_betina' => $sisa_kuota_betina,
                'id_tipe_jadwal' => $id_tipe_jadwal,
                'id_lokasi_jadwal' => $id_lokasi_jadwal,
                'id_status_aktif' => $id_status_aktif,
                'id_status_jadwal' => $id_status_jadwal

            );

            $action_jadwal = $this->Jadwal_model->updatejadwal($id_jadwal, $jadwal);

            if ($action_jadwal) {
                if ($id_user != null) {
                    $this->Kru_model->deleteKruByIdJadwal($id_jadwal);
                    $array_id_user = explode('|', $id_user);
                    for ($x = 0; $x < count($array_id_user); $x++) {
                        $kru = array(
                            'id_jadwal' => $id_jadwal,
                            'id_user' => $array_id_user[$x],
                            'id_tipe_kru' => "1",
                        );
                        if ($array_id_user[$x] != "") {
                            $this->Kru_model->insertkru($kru);
                        }
                    }
                }
                $response['isSuccess'] = true;
                $response['message'] = "berhasil merubah jadwal";
            } else {
                $response['message'] = "gagal merubah jadwal";
            }
        }
        echo json_encode($response);
    }


}
