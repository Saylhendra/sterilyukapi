<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book_versi_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Jadwal_model', '', TRUE);
        $this->load->model('Kru_model', '', TRUE);
        $this->load->model('Status_book_model', '', TRUE);
        $this->load->model('Book_model', '', TRUE);
        $this->load->model('To_Book_model', '', TRUE);
        $this->load->model('Jadwal_block_user_model', '', TRUE);
        $this->load->model('Pembayaran_pendaftaran_peserta_model', '', TRUE);


    }

    public function index()
    {
        echo "Access Denied";
    }

    function all_book()
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['book'] = $this->Book_model->get_all_book();
        echo json_encode($response);
    }

    function book($page = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['book'] = $this->Book_model->get_book($page);
        echo json_encode($response);
    }

    function get_manage_book($id_jadwal, $id_status_book, $page)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['book'] = $this->Book_model->get_manage_book($id_jadwal, $id_status_book, $page);
        echo json_encode($response);
    }


    function prepare_manage_peserta($id_jadwal)
    {
        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($id_jadwal != null) {
            $id_level_user = null;
            $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_level_user);
            if ($detail_jadwal == null) {
                $response['isSuccess'] = false;
                $response['message'] = "not available";
            } else {
                $response['isSuccess'] = true;
                $response['message'] = "berhasil";

                $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
                $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
                $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
                $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
                $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
                $response['biaya_steril_jantan_lokal'] = $detail_jadwal["biaya_steril_jantan_lokal"];
                $response['biaya_steril_jantan_mix'] = $detail_jadwal["biaya_steril_jantan_mix"];
                $response['biaya_steril_betina_lokal'] = $detail_jadwal["biaya_steril_betina_lokal"];
                $response['biaya_steril_betina_mix'] = $detail_jadwal["biaya_steril_betina_mix"];
                $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
                $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
                $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
                $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
                $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
                $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
                $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
                $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
                $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
                $response['id_status_jadwal'] = $detail_jadwal["id_status_jadwal"];
                $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
                $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
                $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
                $response['nama_status_jadwal'] = $detail_jadwal["nama_status_jadwal"];

                $response['kru_jadwal'] = $this->Kru_model->get_all_kru_byIdJadwal($id_jadwal);

                $response['status_book'] = $this->Status_book_model->get_all_status_book();
            }
        } else {
            $response['message'] = "Tidak ada jadwal yang terpilih";
        }
        echo json_encode($response);
    }

    function check_book_by_id_book($id_book)
    {
        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($id_book != null) {
            $id_level_user = null;
            $detail_book = $this->Book_model->getBookById($id_book);
            if ($detail_book == null) {
                $response['message'] = "Tidak ada booking";
                echo json_encode($response);
                return;
            }
            $id_jadwal = $detail_book['id_jadwal'];

            $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_level_user);
            $detail_book = $this->Book_model->get_book_id_book($id_book);

            if ($detail_jadwal == null) {
                $response['message'] = "not available";
            } else {
                $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
                $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
                $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
                $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
                $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
                $response['biaya_steril_jantan_lokal'] = $detail_jadwal["biaya_steril_jantan_lokal"];
                $response['biaya_steril_jantan_mix'] = $detail_jadwal["biaya_steril_jantan_mix"];
                $response['biaya_steril_betina_lokal'] = $detail_jadwal["biaya_steril_betina_lokal"];
                $response['biaya_steril_betina_mix'] = $detail_jadwal["biaya_steril_betina_mix"];
                $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
                $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
                $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
                $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
                $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
                $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
                $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
                $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
                $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
                $response['id_status_jadwal'] = $detail_jadwal["id_status_jadwal"];
                $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
                $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
                $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
                $response['nama_status_jadwal'] = $detail_jadwal["nama_status_jadwal"];
                $response['kru_jadwal'] = $this->Kru_model->get_all_kru_byIdJadwal($id_jadwal);

                if ($detail_book == null) {
                    $response['message'] = "tidak ada book";
                } else {
                    $response['isSuccess'] = true;
                    $response['message'] = "berhasil";

                    $response['id_book'] = $detail_book["id_book"];
                    $response['kode_book'] = $detail_book["kode_book"];
                    $response['id_jadwal'] = $detail_book["id_jadwal"];
                    $response['id_user'] = $detail_book["id_user"];
                    $response['biaya_book'] = $detail_book["biaya_book"];
                    $response['kuota_jantan_book'] = $detail_book["kuota_jantan_book"];
                    $response['kuota_jantan_lokal_book'] = $detail_book["kuota_jantan_lokal_book"];
                    $response['kuota_jantan_mix_book'] = $detail_book["kuota_jantan_mix_book"];
                    $response['kuota_betina_book'] = $detail_book["kuota_betina_book"];
                    $response['kuota_betina_lokal_book'] = $detail_book["kuota_betina_lokal_book"];
                    $response['kuota_betina_mix_book'] = $detail_book["kuota_betina_mix_book"];
                    $response['waktu_book'] = $detail_book["waktu_book"];
                    $response['id_status_book'] = $detail_book["id_status_book"];
                    $response['nama_lengkap'] = $detail_book["nama_lengkap"];
                    $response['alamat'] = $detail_book["alamat"];
                    $response['foto_profil'] = $detail_book["foto_profil"];
                    $response['nama_status_book'] = $detail_book["nama_status_book"];
                    $response['id_pembayaran_pendaftaran_peserta'] = $detail_book["id_pembayaran_pendaftaran_peserta"];
                    $response['id_bank_tujuan_pembayaran'] = $detail_book["id_bank_tujuan_pembayaran"];
                    $response['bank_yang_digunakan'] = $detail_book["bank_yang_digunakan"];
                    $response['nomor_rekening_bank_yang_digunakan'] = $detail_book["nomor_rekening_bank_yang_digunakan"];
                    $response['nama_pemilik_nomor_rekening_bank_yang_digunakan'] = $detail_book["nama_pemilik_nomor_rekening_bank_yang_digunakan"];
                    $response['tanggal_transfer'] = $detail_book["tanggal_transfer"];
                    $response['keterangan'] = $detail_book["keterangan"];
                    $response['waktu_pembayaran_pendaftaran_peserta'] = $detail_book["waktu_pembayaran_pendaftaran_peserta"];
                    $response['foto_bukti_pembayaran'] = $detail_book["foto_bukti_pembayaran"];
                    $response['id_status_pembayaran_pendaftaran_peserta'] = $detail_book["id_status_pembayaran_pendaftaran_peserta"];
                    $response['nama_status_pembayaran_pendaftaran_peserta'] = $detail_book["nama_status_pembayaran_pendaftaran_peserta"];
                    $response['nama_bank_tujuan_pembayaran'] = $detail_book["nama_bank_tujuan_pembayaran"];
                    $response['nomor_rekening_bank_tujuan_pembayaran'] = $detail_book["nomor_rekening_bank_tujuan_pembayaran"];
                    $response['atas_nama_bank_tujuan_pembayaran'] = $detail_book["atas_nama_bank_tujuan_pembayaran"];

                }

            }
        } else {
            $response['message'] = "Tidak ada jadwal yang terpilih";
        }
        echo json_encode($response);
    }

    function actionpeserta()
    {
        $id_book = $this->input->post('id_book');
        $id_status_book = $this->input->post('id_status_book');

        if ($id_status_book == '4') {

        }

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($id_book != null && $id_status_book != null) {

            $book = array(
                'id_status_book' => $id_status_book
            );

            $action_book = $this->Book_model->updatebook($id_book, $book);
            if ($action_book) {

                if ($id_status_book == '4') {
                    $detail_book = $this->Book_model->getBookById($id_book);
                    $detail_jadwal = $this->Jadwal_model->getJadwalById($detail_book["id_jadwal"], $id_user = null);

                    $sisa_kuota_jantan = ((int)$detail_jadwal["sisa_kuota_jantan"]) - ((int)$detail_book["kuota_jantan_book"]);
                    $sisa_kuota_betina = ((int)$detail_jadwal["sisa_kuota_betina"]) - ((int)$detail_book["kuota_betina_book"]);
                    $jumlah_book = ((int)$detail_jadwal["jumlah_book"]) + 1;

                    $jadwal = array(
                        'sisa_kuota_jantan' => $sisa_kuota_jantan,
                        'sisa_kuota_betina' => $sisa_kuota_betina,
                        'jumlah_book' => $jumlah_book,
                    );

                    $pembayaran_pendaftaran_peserta = array(
                        'id_status_pembayaran_pendaftaran_peserta' => '2'
                    );

                    $this->Jadwal_model->updatejadwal($detail_book["id_jadwal"], $jadwal);
                    $this->Pembayaran_pendaftaran_peserta_model->updatepembayaran_pendaftaran_pesertaByIdBook($id_book, $pembayaran_pendaftaran_peserta);
                }

                $response['isSuccess'] = true;
                $response['message'] = "berhasil";
            }
        }
        echo json_encode($response);
    }


    function detail_book($id_book)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_book = $this->Book_model->getBookById($id_book);
        if ($detail_book == null) {
            $response['isSuccess'] = false;
            $response['message'] = "not available";
        }
        $response['id_book'] = $detail_book["id_book"];
        $response['kuota_jantan_book'] = $detail_book["kuota_jantan_book"];
        $response['kuota_betina_book'] = $detail_book["kuota_betina_book"];
        echo json_encode($response);
    }

    function addeditbook()
    {
        $kd = $this->Book_model->getNewKodeBook();
        $kode_book = "#BJ" . ((int)str_replace("#BJ", "", $kd["kode_book"]) + 1);
        $id_book = $this->input->post('id_book');
        $id_jadwal = $this->input->post('id_jadwal');
        $id_user = $this->input->post('id_user');
        $id_level_user = $this->input->post('id_level_user');
        $kuota_jantan_book = $this->input->post('kuota_jantan_book');
        $kuota_jantan_lokal_book = $this->input->post('kuota_jantan_lokal_book');
        $kuota_jantan_mix_book = $this->input->post('kuota_jantan_mix_book');
        $kuota_betina_book = $this->input->post('kuota_betina_book');
        $kuota_betina_lokal_book = $this->input->post('kuota_betina_lokal_book');
        $kuota_betina_mix_book = $this->input->post('kuota_betina_mix_book');
        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($kuota_jantan_book != null || $kuota_betina_book != null) {
            $book = array(
                'id_jadwal' => $id_jadwal,
                'kode_book' => $kode_book,
                'id_user' => $id_user,
                'kuota_jantan_book' => $kuota_jantan_book,
                'kuota_jantan_lokal_book' => $kuota_jantan_lokal_book,
                'kuota_jantan_mix_book' => $kuota_jantan_mix_book,
                'kuota_betina_book' => $kuota_betina_book,
                'kuota_betina_lokal_book' => $kuota_betina_lokal_book,
                'kuota_betina_mix_book' => $kuota_betina_mix_book,

            );

            $nb = $this->Book_model->cekAccessBook($id_jadwal, $id_user);

            if ($id_book != null) {
                //edit
                if ($nb != null) {
                    if ($nb["id_status_book"] == "1") {
                        $action_book = $this->Book_model->updatebook($id_book, $book);
                        if ($action_book) {
                            $response['isSuccess'] = true;
                            $response['message'] = "berhasil mengedit book";
                            $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_user);

                            if ($detail_jadwal == null) {
                                $response['isSuccess'] = false;
                                $response['message'] = "not available";
                            } else {

                                $kru_jadwal = $this->Kru_model->get_kru($id_jadwal, 1, 3);
                                $book_jadwal = $this->Book_model->get_book($id_level_user, $id_jadwal, 1, 3);
                                $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
                                $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
                                $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
                                $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
                                $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
                                $response['biaya_steril_jantan_lokal'] = $detail_jadwal["biaya_steril_jantan_lokal"];
                                $response['biaya_steril_jantan_mix'] = $detail_jadwal["biaya_steril_jantan_mix"];
                                $response['biaya_steril_betina_lokal'] = $detail_jadwal["biaya_steril_betina_lokal"];
                                $response['biaya_steril_betina_mix'] = $detail_jadwal["biaya_steril_betina_mix"];
                                $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
                                $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
                                $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
                                $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
                                $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
                                $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
                                $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
                                $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
                                $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
                                $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
                                $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
                                $response['nama_tipe_jadwal'] = $detail_jadwal["nama_tipe_jadwal"];
                                $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
                                $response['alamat_lokasi_jadwal'] = $detail_jadwal["alamat_lokasi_jadwal"];
                                $response['rute_lokasi_jadwal'] = $detail_jadwal["rute_lokasi_jadwal"];
                                $response['no_telp_lokasi_jadwal'] = $detail_jadwal["no_telp_lokasi_jadwal"];
                                $response['latitude_lokasi_jadwal'] = $detail_jadwal["latitude_lokasi_jadwal"];
                                $response['longitude_lokasi_jadwal'] = $detail_jadwal["longitude_lokasi_jadwal"];
                                $response['id_status_jadwal'] = $detail_jadwal["id_status_jadwal"];
                                $response['nama_status_jadwal'] = $detail_jadwal["nama_status_jadwal"];
                                $response['id_user'] = $detail_jadwal["id_user"];
                                $response['id_book'] = $detail_jadwal["id_book"];
                                $response['id_status_book'] = $detail_jadwal["id_status_book"];
                                $response['kuota_jantan_book'] = $detail_jadwal["kuota_jantan_book"];
                                $response['kuota_jantan_lokal_book'] = $detail_jadwal["kuota_jantan_lokal_book"];
                                $response['kuota_jantan_mix_book'] = $detail_jadwal["kuota_jantan_mix_book"];
                                $response['kuota_betina_book'] = $detail_jadwal["kuota_betina_book"];
                                $response['kuota_betina_lokal_book'] = $detail_jadwal["kuota_betina_lokal_book"];
                                $response['kuota_betina_mix_book'] = $detail_jadwal["kuota_betina_mix_book"];
                                $response['waktu_book'] = $detail_jadwal["waktu_book"];
                                $response['nama_status_book'] = $detail_jadwal["nama_status_book"];
                                $response['isToBook'] = $this->To_Book_model->check_exitst_by_id_jadwal_id_user($id_jadwal, $id_user);
                                $response['is_blocked_user'] = $this->Jadwal_block_user_model->check_exitst_by_id_jadwal_id_user($id_jadwal, $id_user);
                                $response['kru'] = $kru_jadwal;
                                $response['book'] = $book_jadwal;
                            }
                        } else {
                            $response['message'] = "gagal mengedit book";
                        }
                    } else {
                        $response['message'] = "gagal mengedit book";
                    }
                } else {
                    $response['message'] = "gagal menambah book";
                }

            } else {
                $action_book = $this->Book_model->insertbook($book);
                if ($action_book) {

                    $this->To_Book_model->delete_to_book_by_id_jadwal_id_user($id_jadwal, $id_user);

                    $response['isSuccess'] = true;
                    $response['message'] = "berhasil menambah book";

                    $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_user);

                    if ($detail_jadwal == null) {
                        $response['isSuccess'] = false;
                        $response['message'] = "not available";
                    } else {
                        $kru_jadwal = $this->Kru_model->get_kru($id_jadwal, 1, 3);
                        $book_jadwal = $this->Book_model->get_book($id_level_user, $id_jadwal, 1, 3);
                        $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
                        $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
                        $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
                        $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
                        $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
                        $response['biaya_steril_jantan_lokal'] = $detail_jadwal["biaya_steril_jantan_lokal"];
                        $response['biaya_steril_jantan_mix'] = $detail_jadwal["biaya_steril_jantan_mix"];
                        $response['biaya_steril_betina_lokal'] = $detail_jadwal["biaya_steril_betina_lokal"];
                        $response['biaya_steril_betina_mix'] = $detail_jadwal["biaya_steril_betina_mix"];
                        $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
                        $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
                        $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
                        $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
                        $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
                        $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
                        $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
                        $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
                        $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
                        $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
                        $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
                        $response['nama_tipe_jadwal'] = $detail_jadwal["nama_tipe_jadwal"];
                        $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
                        $response['alamat_lokasi_jadwal'] = $detail_jadwal["alamat_lokasi_jadwal"];
                        $response['rute_lokasi_jadwal'] = $detail_jadwal["rute_lokasi_jadwal"];
                        $response['no_telp_lokasi_jadwal'] = $detail_jadwal["no_telp_lokasi_jadwal"];
                        $response['latitude_lokasi_jadwal'] = $detail_jadwal["latitude_lokasi_jadwal"];
                        $response['longitude_lokasi_jadwal'] = $detail_jadwal["longitude_lokasi_jadwal"];
                        $response['id_status_jadwal'] = $detail_jadwal["id_status_jadwal"];
                        $response['nama_status_jadwal'] = $detail_jadwal["nama_status_jadwal"];
                        $response['id_user'] = $detail_jadwal["id_user"];
                        $response['id_book'] = $detail_jadwal["id_book"];
                        $response['kode_book'] = $detail_jadwal["kode_book"];
                        $response['id_status_book'] = $detail_jadwal["id_status_book"];
                        $response['biaya_book'] = $detail_jadwal["biaya_book"];
                        $response['kuota_jantan_book'] = $detail_jadwal["kuota_jantan_book"];
                        $response['kuota_jantan_lokal_book'] = $detail_jadwal["kuota_jantan_lokal_book"];
                        $response['kuota_jantan_mix_book'] = $detail_jadwal["kuota_jantan_mix_book"];
                        $response['kuota_betina_book'] = $detail_jadwal["kuota_betina_book"];
                        $response['kuota_betina_lokal_book'] = $detail_jadwal["kuota_betina_lokal_book"];
                        $response['kuota_betina_mix_book'] = $detail_jadwal["kuota_betina_mix_book"];
                        $response['waktu_book'] = $detail_jadwal["waktu_book"];
                        $response['nama_status_book'] = $detail_jadwal["nama_status_book"];
                        $response['id_pembayaran_pendaftaran_peserta'] = $detail_jadwal["id_pembayaran_pendaftaran_peserta"];
                        $response['id_bank_tujuan_pembayaran'] = $detail_jadwal["id_bank_tujuan_pembayaran"];
                        $response['bank_yang_digunakan'] = $detail_jadwal["bank_yang_digunakan"];
                        $response['nomor_rekening_bank_yang_digunakan'] = $detail_jadwal["nomor_rekening_bank_yang_digunakan"];
                        $response['nama_pemilik_nomor_rekening_bank_yang_digunakan'] = $detail_jadwal["nama_pemilik_nomor_rekening_bank_yang_digunakan"];
                        $response['tanggal_transfer'] = $detail_jadwal["tanggal_transfer"];
                        $response['keterangan'] = $detail_jadwal["keterangan"];
                        $response['waktu_pembayaran_pendaftaran_peserta'] = $detail_jadwal["waktu_pembayaran_pendaftaran_peserta"];
                        $response['foto_bukti_pembayaran'] = $detail_jadwal["foto_bukti_pembayaran"];
                        $response['id_status_pembayaran_pendaftaran_peserta'] = $detail_jadwal["id_status_pembayaran_pendaftaran_peserta"];
                        $response['nama_status_pembayaran_pendaftaran_peserta'] = $detail_jadwal["nama_status_pembayaran_pendaftaran_peserta"];
                        $response['nama_bank_tujuan_pembayaran'] = $detail_jadwal["nama_status_book"];
                        $response['nomor_rekening_bank_tujuan_pembayaran'] = $detail_jadwal["nomor_rekening_bank_tujuan_pembayaran"];
                        $response['atas_nama_bank_tujuan_pembayaran'] = $detail_jadwal["atas_nama_bank_tujuan_pembayaran"];
                        $response['isToBook'] = $this->To_Book_model->check_exitst_by_id_jadwal_id_user($id_jadwal, $id_user);
                        $response['is_blocked_user'] = $this->Jadwal_block_user_model->check_exitst_by_id_jadwal_id_user($id_jadwal, $id_user);
                        $response['kru'] = $kru_jadwal;
                        $response['book'] = $book_jadwal;
                    }
                } else {
                    $response['message'] = "gagal menambah book";
                }
            }
        }
        echo json_encode($response);
    }

    function delete_book($id)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil menghapus book";
        $this->Book_model->delete_book($id);
        echo json_encode($response);
    }


    function batalbook($id_user, $id_jadwal, $id_book)
    {
        $response['isSuccess'] = true;
        $response['message'] = "Kepersertaan berhasil dibatalkan";
        $batalbook = $this->Book_model->update_status_book($id_user, $id_jadwal, $id_book, "5");

        if ($batalbook) {

            $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_user);
            $kru_jadwal = $this->Kru_model->get_kru($id_jadwal, 1, 3);
            $book_jadwal = $this->Book_model->get_book("3", $id_jadwal, 1, 3);

            $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
            $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
            $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
            $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
            $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
            $response['biaya_steril_jantan_lokal'] = $detail_jadwal["biaya_steril_jantan_lokal"];
            $response['biaya_steril_jantan_mix'] = $detail_jadwal["biaya_steril_jantan_mix"];
            $response['biaya_steril_betina_lokal'] = $detail_jadwal["biaya_steril_betina_lokal"];
            $response['biaya_steril_betina_mix'] = $detail_jadwal["biaya_steril_betina_mix"];
            $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
            $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
            $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
            $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
            $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
            $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
            $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
            $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
            $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
            $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
            $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
            $response['nama_tipe_jadwal'] = $detail_jadwal["nama_tipe_jadwal"];
            $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
            $response['alamat_lokasi_jadwal'] = $detail_jadwal["alamat_lokasi_jadwal"];
            $response['rute_lokasi_jadwal'] = $detail_jadwal["rute_lokasi_jadwal"];
            $response['no_telp_lokasi_jadwal'] = $detail_jadwal["no_telp_lokasi_jadwal"];
            $response['latitude_lokasi_jadwal'] = $detail_jadwal["latitude_lokasi_jadwal"];
            $response['longitude_lokasi_jadwal'] = $detail_jadwal["longitude_lokasi_jadwal"];
            $response['id_status_jadwal'] = $detail_jadwal["id_status_jadwal"];
            $response['nama_status_jadwal'] = $detail_jadwal["nama_status_jadwal"];
            $response['id_user'] = $detail_jadwal["id_user"];
            $response['id_book'] = $detail_jadwal["id_book"];
            $response['kode_book'] = $detail_jadwal["kode_book"];
            $response['id_status_book'] = $detail_jadwal["id_status_book"];
            $response['biaya_book'] = $detail_jadwal["biaya_book"];
            $response['kuota_jantan_book'] = $detail_jadwal["kuota_jantan_book"];
            $response['kuota_jantan_lokal_book'] = $detail_jadwal["kuota_jantan_lokal_book"];
            $response['kuota_jantan_mix_book'] = $detail_jadwal["kuota_jantan_mix_book"];
            $response['kuota_betina_book'] = $detail_jadwal["kuota_betina_book"];
            $response['kuota_betina_lokal_book'] = $detail_jadwal["kuota_betina_lokal_book"];
            $response['kuota_betina_mix_book'] = $detail_jadwal["kuota_betina_mix_book"];
            $response['waktu_book'] = $detail_jadwal["waktu_book"];
            $response['nama_status_book'] = $detail_jadwal["nama_status_book"];
            $response['id_pembayaran_pendaftaran_peserta'] = $detail_jadwal["id_pembayaran_pendaftaran_peserta"];
            $response['id_bank_tujuan_pembayaran'] = $detail_jadwal["id_bank_tujuan_pembayaran"];
            $response['bank_yang_digunakan'] = $detail_jadwal["bank_yang_digunakan"];
            $response['nomor_rekening_bank_yang_digunakan'] = $detail_jadwal["nomor_rekening_bank_yang_digunakan"];
            $response['nama_pemilik_nomor_rekening_bank_yang_digunakan'] = $detail_jadwal["nama_pemilik_nomor_rekening_bank_yang_digunakan"];
            $response['tanggal_transfer'] = $detail_jadwal["tanggal_transfer"];
            $response['keterangan'] = $detail_jadwal["keterangan"];
            $response['waktu_pembayaran_pendaftaran_peserta'] = $detail_jadwal["waktu_pembayaran_pendaftaran_peserta"];
            $response['foto_bukti_pembayaran'] = $detail_jadwal["foto_bukti_pembayaran"];
            $response['id_status_pembayaran_pendaftaran_peserta'] = $detail_jadwal["id_status_pembayaran_pendaftaran_peserta"];
            $response['nama_status_pembayaran_pendaftaran_peserta'] = $detail_jadwal["nama_status_pembayaran_pendaftaran_peserta"];
            $response['nama_bank_tujuan_pembayaran'] = $detail_jadwal["nama_status_book"];
            $response['nomor_rekening_bank_tujuan_pembayaran'] = $detail_jadwal["nomor_rekening_bank_tujuan_pembayaran"];
            $response['atas_nama_bank_tujuan_pembayaran'] = $detail_jadwal["atas_nama_bank_tujuan_pembayaran"];
            $response['isToBook'] = $this->To_Book_model->check_exitst_by_id_jadwal_id_user($id_jadwal, $id_user);
            $response['is_blocked_user'] = $this->Jadwal_block_user_model->check_exitst_by_id_jadwal_id_user($id_jadwal, $id_user);
            $response['kru'] = $kru_jadwal;
            $response['book'] = $book_jadwal;
        } else {
            $response['isSuccess'] = false;
            $response['message'] = "Gagal membatalkan kepesertaan";
        }
        echo json_encode($response);
    }


}
