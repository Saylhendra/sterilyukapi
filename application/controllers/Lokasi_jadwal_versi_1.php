<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi_jadwal_versi_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Lokasi_jadwal_model', '', TRUE);

    }

    public function index()
    {
        echo "Access Denied";
    }

    function detail_lokasi_jadwal($id_lokasi_jadwal)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_lokasi_jadwal = $this->Lokasi_jadwal_model->getLokasiJadwalById($id_lokasi_jadwal);
        $response['id_lokasi_jadwal'] = $detail_lokasi_jadwal["id_lokasi_jadwal"];
        $response['nama_lokasi_jadwal'] = $detail_lokasi_jadwal["nama_lokasi_jadwal"];
        $response['alamat_lokasi_jadwal'] = $detail_lokasi_jadwal["alamat_lokasi_jadwal"];
        $response['rute_lokasi_jadwal'] = $detail_lokasi_jadwal["rute_lokasi_jadwal"];
        $response['no_telp_lokasi_jadwal'] = $detail_lokasi_jadwal["no_telp_lokasi_jadwal"];
        $response['latitude_lokasi_jadwal'] = $detail_lokasi_jadwal["latitude_lokasi_jadwal"];
        $response['longitude_lokasi_jadwal'] = $detail_lokasi_jadwal["longitude_lokasi_jadwal"];
        echo json_encode($response);
    }

    function lokasi_jadwal($page = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['lokasi_jadwal'] = $this->Lokasi_jadwal_model->get_lokasi_jadwal($page);
        echo json_encode($response);
    }

    function all_lokasi_jadwal()
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['lokasi_jadwal'] = $this->Lokasi_jadwal_model->get_all_lokasi_jadwal();
        echo json_encode($response);
    }

    function addeditlokasi_jadwal()
    {
        $id_lokasi_jadwal = $this->input->post('id_lokasi_jadwal');
        $nama_lokasi_jadwal = $this->input->post('nama_lokasi_jadwal');
        $alamat_lokasi_jadwal = $this->input->post('alamat_lokasi_jadwal');
        $rute_lokasi_jadwal = $this->input->post('rute_lokasi_jadwal');
        $no_telp_lokasi_jadwal = $this->input->post('no_telp_lokasi_jadwal');
        $latitude_lokasi_jadwal = $this->input->post('latitude_lokasi_jadwal');
        $longitude_lokasi_jadwal = $this->input->post('longitude_lokasi_jadwal');

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($nama_lokasi_jadwal != null || $alamat_lokasi_jadwal != null|| $rute_lokasi_jadwal != null || $no_telp_lokasi_jadwal != null || $latitude_lokasi_jadwal != null || $longitude_lokasi_jadwal != null) {
            $lokasi_jadwal = array(
                'nama_lokasi_jadwal' => $nama_lokasi_jadwal,
                'alamat_lokasi_jadwal' => $alamat_lokasi_jadwal,
                'rute_lokasi_jadwal' => $rute_lokasi_jadwal,
                'no_telp_lokasi_jadwal' => $no_telp_lokasi_jadwal,
                'latitude_lokasi_jadwal' => $latitude_lokasi_jadwal,
                'longitude_lokasi_jadwal' => $longitude_lokasi_jadwal

            );

            $nb = $this->Lokasi_jadwal_model->ceknamalokasi_jadwal($nama_lokasi_jadwal);

            if ($id_lokasi_jadwal != null) {
                if (strtolower($nb["nama_lokasi_jadwal"]) == strtolower($nama_lokasi_jadwal) || $nb == null) {
                    $action_lokasi_jadwal = $this->Lokasi_jadwal_model->updatelokasi_jadwal($id_lokasi_jadwal, $lokasi_jadwal);
                    if ($action_lokasi_jadwal) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil mengedit lokasi_jadwal";
                    } else {
                        $response['message'] = "gagal mengedit lokasi_jadwal";
                    }
                } else {
                    $response['message'] = "Nama Lokasi sudah ada...";
                }

            } else {
                if ($nb != null) {
                    $response['message'] = "Nama Lokasi sudah ada...";
                } else {
                    $action_lokasi_jadwal = $this->Lokasi_jadwal_model->insertlokasi_jadwal($lokasi_jadwal);
                    if ($action_lokasi_jadwal) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil menambah lokasi_jadwal";
                    } else {
                        $response['message'] = "gagal menambah lokasi_jadwal";
                    }
                }
            }
        }
        echo json_encode($response);
    }

    function delete_lokasi_jadwal($id)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil menghapus lokasi_jadwal";
        $this->Lokasi_jadwal_model->delete_lokasi_jadwal($id);
        echo json_encode($response);
    }


}
