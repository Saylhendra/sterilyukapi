<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property User_model $User_model
 */
class User_versi_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('User_model', '', TRUE);

    }

    function user($page = null, $search = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['user'] = $this->User_model->get_user($page, $search);
        echo json_encode($response);
    }

    function role($page = null, $search = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['user'] = $this->User_model->get_role($page, $search);
        echo json_encode($response);
    }

    function detail_user($id_user)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_user = $this->User_model->getUserById($id_user);
        if ($detail_user == null) {
            $response['isSuccess'] = false;
            $response['message'] = "not available";
        } else {
            $arr_tanggal_lahir = explode("-", $detail_user["tanggal_lahir"]);
            $new_tanggal_lahir = $arr_tanggal_lahir[0] . "-" . $arr_tanggal_lahir[1] . "-" . $arr_tanggal_lahir[1];

            $response["id_user"] = $detail_user["id_user"];
            $response["nama_lengkap"] = $detail_user["nama_lengkap"];
            $response["jenis_kelamin"] = $detail_user["jenis_kelamin"];
            $response["tempat_lahir"] = $detail_user["tempat_lahir"];
            $response["tanggal_lahir"] = $new_tanggal_lahir;
            $response["alamat"] = $detail_user["alamat"];
            $response["no_telp"] = $detail_user["no_telp"];
            $response["email"] = $detail_user["email"];
            $response["foto_profil"] = $detail_user["foto_profil"];

            $response["id_firebase"] = $detail_user["id_firebase"];
            $response["id_level_user"] = $detail_user["id_level_user"];
            $response["id_status_aktif"] = $detail_user["id_status_aktif"];
        }

        echo json_encode($response);
    }

    function delete_user($id)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil menghapus user";
        $this->User_model->delete_user($id);
        echo json_encode($response);
    }

    function addedituser()
    {

        $id_user = $this->input->post('id_user');
        $nama_lengkap = $this->input->post('nama_lengkap');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $tanggal_lahir = $this->input->post('tanggal_lahir');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $no_telp = $this->input->post('no_telp');
        $alamat = $this->input->post('alamat');
        $email = $this->input->post('email');
        $foto_profil = $this->input->post('foto_profil');
        $id_status_aktif = $this->input->post('id_status_aktif');

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($nama_lengkap != null
            || $jenis_kelamin != null
            || $tanggal_lahir != null
            || $tempat_lahir != null
            || $no_telp != null
            || $alamat != null
            || $email != null
        ) {

            $kb = $this->User_model->cekemail($email);
            if ($id_user != null) {
                if (strtolower($kb["email"]) == strtolower($email) || $kb == null) {

                    if ($foto_profil == null) {
                        if ($id_status_aktif != null) {
                            $user = array(
                                'nama_lengkap' => $nama_lengkap,
                                'jenis_kelamin' => $jenis_kelamin,
                                'tanggal_lahir' => $tanggal_lahir,
                                'tempat_lahir' => $tempat_lahir,
                                'no_telp' => $no_telp,
                                'alamat' => $alamat,
                                'email' => $email,
                                'id_status_aktif' => id_status_aktif
                            );
                        } else {
                            $user = array(
                                'nama_lengkap' => $nama_lengkap,
                                'jenis_kelamin' => $jenis_kelamin,
                                'tanggal_lahir' => $tanggal_lahir,
                                'tempat_lahir' => $tempat_lahir,
                                'no_telp' => $no_telp,
                                'alamat' => $alamat,
                                'email' => $email,
                            );
                        }

                    } else {

                        $image = base64_decode(str_replace('data:image/jpg;base64,', '', $foto_profil));
                        $upload_path = APPPATH . '../source/upload/image/foto_profil/';
                        $new_name = time();
                        $file_name = $new_name;

                        if (file_put_contents($upload_path . $file_name . ".jpg", $image)) {
                            $url_foto_profil = "/source/upload/image/foto_profil/" . $file_name . ".jpg";

                        } else {
                            echo json_encode($response);
                            return;
                        }
                        if ($id_status_aktif != null) {
                            $user = array(
                                'nama_lengkap' => $nama_lengkap,
                                'jenis_kelamin' => $jenis_kelamin,
                                'tanggal_lahir' => $tanggal_lahir,
                                'tempat_lahir' => $tempat_lahir,
                                'no_telp' => $no_telp,
                                'alamat' => $alamat,
                                'email' => $email,
                                'id_status_aktif' => id_status_aktif,
                                'foto_profil' => $url_foto_profil
                            );
                        } else {
                            $user = array(
                                'nama_lengkap' => $nama_lengkap,
                                'jenis_kelamin' => $jenis_kelamin,
                                'tanggal_lahir' => $tanggal_lahir,
                                'tempat_lahir' => $tempat_lahir,
                                'no_telp' => $no_telp,
                                'alamat' => $alamat,
                                'email' => $email,
                                'foto_profil' => $url_foto_profil
                            );
                        }

                    }


                    $action_user = $this->User_model->updateuser($id_user, $user);

                    if ($action_user) {
                        $user = $this->User_model->getUserByEmail($email);
                        if ($user != null) {
                            $arr_tanggal_lahir = explode("-", $user["tanggal_lahir"]);
                            $new_tanggal_lahir = $arr_tanggal_lahir[0] . "-" . $arr_tanggal_lahir[1] . "-" . $arr_tanggal_lahir[1];
                            $response['isSuccess'] = true;
                            $response['message'] = "berhasil mengedit user";
                            $response["id_user"] = $user["id_user"];
                            $response["nama_lengkap"] = $user["nama_lengkap"];
                            $response["jenis_kelamin"] = $user["jenis_kelamin"];
                            $response["tempat_lahir"] = $user["tempat_lahir"];
                            $response["tanggal_lahir"] = $new_tanggal_lahir;
                            $response["alamat"] = $user["alamat"];
                            $response["no_telp"] = $user["no_telp"];
                            $response["email"] = $user["email"];
                            $response["real_password"] = $user["real_password"];
                            $response["foto_profil"] = $user["foto_profil"];
                            $response["id_firebase"] = $user["id_firebase"];
                            $response["id_level_user"] = $user["id_level_user"];
                            $response["id_status_aktif"] = $user["id_status_aktif"];
                        } else {
                            $response['message'] = "gagal mengedit user";
                        }
                    } else {
                        $response['message'] = "gagal mengedit user";
                    }

                } else {
                    $response['message'] = "Email sudah ada...";
                }

            } else {
                if ($kb != null) {
                    $response['message'] = "Email sudah ada...";
                } else {

                    if ($foto_profil == null) {
                        $url_foto_profil = "";
                    } else {

                        $image = base64_decode(str_replace('data:image/jpg;base64,', '', $foto_profil));
                        $upload_path = APPPATH . '../source/upload/image/foto_profil/';
                        $new_name = time();
                        $file_name = $new_name;

                        if (file_put_contents($upload_path . $file_name . ".jpg", $image)) {
                            $url_foto_profil = "/source/upload/image/foto_profil/" . $file_name . ".jpg";

                        } else {
                            echo json_encode($response);
                            return;
                        }

                    }

                    if ($id_status_aktif != null) {
                        $user = array(
                            'nama_lengkap' => $nama_lengkap,
                            'jenis_kelamin' => $jenis_kelamin,
                            'tanggal_lahir' => $tanggal_lahir,
                            'tempat_lahir' => $tempat_lahir,
                            'no_telp' => $no_telp,
                            'alamat' => $alamat,
                            'email' => $email,
                            'id_status_aktif' => id_status_aktif,
                            'foto_profil' => $url_foto_profil
                        );
                    } else {
                        $user = array(
                            'nama_lengkap' => $nama_lengkap,
                            'jenis_kelamin' => $jenis_kelamin,
                            'tanggal_lahir' => $tanggal_lahir,
                            'tempat_lahir' => $tempat_lahir,
                            'no_telp' => $no_telp,
                            'alamat' => $alamat,
                            'email' => $email,
                            'foto_profil' => $url_foto_profil,
                        );
                    }


                    $action_user = $this->User_model->insertuser($user);
                    if ($action_user) {
                        $result_user = $this->User_model->getUserByEmail($email);
                        if ($result_user != null) {
                            $arr_tanggal_lahir = explode("-", $user["tanggal_lahir"]);
                            $new_tanggal_lahir = $arr_tanggal_lahir[0] . "-" . $arr_tanggal_lahir[1] . "-" . $arr_tanggal_lahir[1];
                            $response['isSuccess'] = true;
                            $response['message'] = "berhasil menambah user";
                            $response["id_user"] = $user["id_user"];
                            $response["nama_lengkap"] = $user["nama_lengkap"];
                            $response["jenis_kelamin"] = $user["jenis_kelamin"];
                            $response["tempat_lahir"] = $user["tempat_lahir"];
                            $response["tanggal_lahir"] = $new_tanggal_lahir;
                            $response["alamat"] = $user["alamat"];
                            $response["no_telp"] = $user["no_telp"];
                            $response["email"] = $user["email"];
                            $response["real_password"] = $user["real_password"];
                            $response["foto_profil"] = $user["foto_profil"];
                            $response["id_firebase"] = $user["id_firebase"];
                            $response["id_level_user"] = $user["id_level_user"];
                            $response["id_status_aktif"] = $user["id_status_aktif"];

                        } else {
                            $response['message'] = "gagal menambah user";
                        }
                    } else {
                        $response['message'] = "gagal menambah user";
                    }
                }
            }
        }
        echo json_encode($response);
    }


    function login()
    {
        $if = $this->input->post('id_firebase');
        $e = $this->input->post('email');
        $p = md5($this->input->post('password'));

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($e != null && $p != null) {
            $user = $this->User_model->login($e, $p);
            if ($user != null) {
                $arr_tanggal_lahir = explode("-", $user["tanggal_lahir"]);
                $new_tanggal_lahir = $arr_tanggal_lahir[0] . "-" . $arr_tanggal_lahir[1] . "-" . $arr_tanggal_lahir[1];
                if ($user["id_status_aktif"] == "1") {
                    $this->User_model->UpdateIdFireBase($if, $user["id_user"]);
                    $response['isSuccess'] = true;
                    $response['message'] = "Login berhasil";
                    $response["id_user"] = $user["id_user"];
                    $response["nama_lengkap"] = $user["nama_lengkap"];
                    $response["jenis_kelamin"] = $user["jenis_kelamin"];
                    $response["tempat_lahir"] = $user["tempat_lahir"];
                    $response["tanggal_lahir"] = $new_tanggal_lahir;
                    $response["alamat"] = $user["alamat"];
                    $response["no_telp"] = $user["no_telp"];
                    $response["email"] = $user["email"];
                    $response["foto_profil"] = $user["foto_profil"];

                    $response["id_firebase"] = $user["id_firebase"];
                    $response["id_level_user"] = $user["id_level_user"];
                    $response["id_status_aktif"] = $user["id_status_aktif"];
                } else {
                    $response['message'] = "User Tidak Aktif . Harap Hubungi Admin";
                }
            } else {
                $response['message'] = "Email atau password anda salah";
            }
        }
        echo json_encode($response);
    }


    function change_password_user()
    {
        $id_user = $this->input->post('id_user');
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));
        $password_baru = md5($this->input->post('password_baru'));

        $data = array(
            'password' => $password,
        );

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($password != null && $password_baru != null
        ) {
            $cekpassword = $this->User_model->cekpassword($id_user, $password);

            if ($cekpassword == null
            ) {
                $response['message'] = "Password lama anda salah";
                echo json_encode($response);
                return;
            }

            if ($cekpassword['password'] == $password_baru
            ) {
                $response['message'] = "Password anda harus berbeda dengan password lama";
                echo json_encode($response);
                return;
            }
            $change_password = $this->User_model->updateuser($id_user, $data);
            if ($change_password) {
                $to = $email;
                $subject = "PASSWORD BERUBAH";
                $message = "
                    <html>
                    <head>
                    <title>PASSWORD BERUBAH</title>
                    </head>
                    <body>
                    <H2>PASSWORD BERUBAH</H2>
                    Password Anda Berhasil dirubah.<br />
                    <br /><br /><br />Thanks<br />Tim SterilYuk
                    </body>
                    </html>
                    ";

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: <no-reply@sterilyuk.com>' . "\r\n";
                mail($to, $subject, $message, $headers);

                $response['isSuccess'] = true;
                $response['message'] = " Password Anda Berhasil dirubah.";


            } else {
                $response['message'] = "Gagal merubah password";
            }

        }
        echo json_encode($response);
    }

    function register()
    {
        $nama_lengkap = $this->input->post('nama_lengkap');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $tanggal_lahir = $this->input->post('tanggal_lahir');

        $arr_tanggal_lahir = explode("-", $tanggal_lahir);
        $new_tanggal_lahir = isset($arr_tanggal_lahir[2]) . "-" . isset($arr_tanggal_lahir[1]) . "-" . isset($arr_tanggal_lahir[0]);
        
        $tempat_lahir = $this->input->post('tempat_lahir');
        $no_telp = $this->input->post('no_telp');
        $email = $this->input->post('email');
        
        $arr_tanggal_lahir = explode("@", $email);
        $nama_from_email = isset($arr_tanggal_lahir[0]);        
        
        $password = md5($this->input->post('password'));
        $real_password = $this->input->post('real_password');
        $id_firebase = $this->input->post('id_firebase');


        $data = array(
            'nama_lengkap' => $nama_from_email,           
            'no_telp' => $no_telp,
            'email' => $email,
            'password' => $password,
            'real_password' => $real_password,
            'id_firebase' => $id_firebase,
        );

        $response['isSuccess'] = false;
        $response['message'] = "Maaf, data tidak lengkap.";        
        
        if ($email != null 
                && $no_telp != null
                    && $password != null
                        && $real_password != null
                            && $id_firebase != null) 
        {               
            $cemail = $this->User_model->cekemail($email);
            if (!isset($cemail)) {
                $register = $this->User_model->register($data);
                if ($register) 
                {
                    $response['isSuccess'] = true;
                    $response['message'] = "berhasil menambah user";
                } else {
                    $response['message'] = "gagal menambah user";
                }
                if ($register) {
                    $to = $email;
                    $subject = "KONFIRMASI EMAIL USER STERILYUK";

                    $message = "
                    <html>
                    <head>
                    <title>KONFIRMASI EMAIL USER STERILYUK</title>
                    </head>
                    <body>
                    <H2>KONFIRMASI EMAIL</H2>
                    Silahkan klik tautan aktivasi di bawah ini atau copy paste pada browser anda untuk memverifikasi email anda.<br />
                    <br /><a href='http://apisterilyuk.amaydiam.com/index.php/user/verify/" . md5($email) . "'> http://apisterilyuk.amaydiam.com/index.php/user/verify/" . md5($email) . "</a>
                    <br /><br /><br />Thanks<br />Tim SterilYuk
                    </body>
                    </html>
                    ";

                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: <no-reply@sterilyuk.com>' . "\r\n";
                    mail($to, $subject, $message, $headers);

                    $response['isSuccess'] = true;
                    $response['message'] = "Silahkan verifikasi email anda.";


                } else {
                    $response['message'] = "Pendaftaran gagal";
                }
            } else {
                $response['message'] = "Maaf, email sudah terdaftar";
            }
        }
        echo json_encode($response);
    }

    function forgotpassword()
    {
        $this->load->helper('email');
        $e = $this->input->post('email');

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($e != null) {
            if (valid_email($e)) {
                $email = $this->User_model->cekemail($e);
                if ($email != null) {
                    $newPassword = $this->randomPassword();
                    $generateNewPass = md5($newPassword);
                    $data = array(
                        'password' => $generateNewPass
                    );

                    $resetPassword = $this->User_model->resetpassword($e, $data);
                    if ($resetPassword) {
                        $to = $e;
                        $subject = "Reset Password Account Streril Yuk";

                        $message = "
<html>
<head>
<title>Reset Password Account Streril Yuk</title>
</head>
<body>
<p>Reset Password Account Streril Yuk</p>
Your new Password Account Streril Yuk now : " . $newPassword . "
</body>
</html>
";

// Always set content-type when sending HTML email
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
                        $headers .= 'From: <noreply@sterilyuk.com>' . "\r\n";

                        mail($to, $subject, $message, $headers);

                        $response['isSuccess'] = true;
                        $response['message'] = "Check your email or Contact Your Admin";
                    }

                } else {
                    $response['message'] = "Email not exist";
                }
            } else {
                $response['message'] = "email is not valid";
            }

        }
        echo json_encode($response);
    }

    function randomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


    function addeditrole()
    {

        $id_user = $this->input->post('id_user');
        $multi_id_user = $this->input->post('multi_id_user');
        $id_level_user = $this->input->post('id_level_user');

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($id_level_user != null
        ) {

            if ($id_user != null) {

                $user = array(
                    'id_level_user' => $id_level_user
                );


                $action_user = $this->User_model->updateuser($id_user, $user);

                if ($action_user) {
                    $user = $this->User_model->getUserById($id_user);
                    if ($user != null) {
                        $arr_tanggal_lahir = explode("-", $user["tanggal_lahir"]);
                        $new_tanggal_lahir = $arr_tanggal_lahir[0] . "-" . $arr_tanggal_lahir[1] . "-" . $arr_tanggal_lahir[1];
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil mengedit user";
                        $response["id_user"] = $user["id_user"];
                        $response["nama_lengkap"] = $user["nama_lengkap"];
                        $response["jenis_kelamin"] = $user["jenis_kelamin"];
                        $response["tempat_lahir"] = $user["tempat_lahir"];
                        $response["tanggal_lahir"] = $new_tanggal_lahir;
                        $response["alamat"] = $user["alamat"];
                        $response["no_telp"] = $user["no_telp"];
                        $response["email"] = $user["email"];
                        $response["real_password"] = $user["real_password"];
                        $response["foto_profil"] = $user["foto_profil"];
                        $response["id_firebase"] = $user["id_firebase"];
                        $response["id_level_user"] = $user["id_level_user"];
                        $response["id_status_aktif"] = $user["id_status_aktif"];
                    } else {
                        $response['message'] = "gagal mengedit role";
                    }
                } else {
                    $response['message'] = "gagal mengedit role";
                }


            } else {

                $wordChunks = explode("|", $multi_id_user);
                $succ = 0;
                for ($i = 0; $i < count($wordChunks); $i++) {
                    if (strlen($wordChunks[$i]) != 0) {

                        $user = array(
                            'id_level_user' => $id_level_user
                        );

                        $action_user = $this->User_model->updateuser($wordChunks[$i], $user);
                        if ($action_user) {
                            $succ++;
                        }
                    }


                }

                if ($succ > 0) {
                    $response['isSuccess'] = true;
                    $response['message'] = "berhasil menambah role";
                } else {
                    $response['message'] = "gagal menambah role";
                }
            }
        }
        echo json_encode($response);
    }

    function delete_role($id_user)
    {

        $user = array(
            'id_level_user' => '3'
        );

        $action_user = $this->User_model->updateuser($id_user, $user);
        if ($action_user) {
            $response['isSuccess'] = true;
            $response['message'] = "berhasil menghapus role user ini";
        } else {
            $response['isSuccess'] = false;
            $response['message'] = "gagal menghapus role user ini";
        }

        echo json_encode($response);
    }


}
