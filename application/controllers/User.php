<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('User_model', '', TRUE);

    }

    function verify($hash = NULL)
    {
        if ($this->User_model->verifyEmailID($hash)) {
            $user = $this->User_model->getUserByHashEmailID($hash);
            $to = $user["email"];
            $subject = "PENDAFTARAN USER STERILYUK BERHASIL";

            $message = "
                    <html>
                    <head>
                    <title>PENDAFTARAN USER STERILYUK BERHASIL</title>
                    </head>
                    <body>
                    <H2>PENDAFTARAN USER STERILYUK BERHASIL</H2>
                   Terima Kasih, Anda telah terdaftar sebagai user SterilYuk.
                   <br /><br /><br />Thanks<br />Tim SterilYuk
                    </body>
                    </html>
                    ";

            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: <no-reply@sterilyuk.com>' . "\r\n";
            mail($to, $subject, $message, $headers);

            echo "Email Anda berhasil diverifikasi. Silakan masuk ke akun Anda.";
        } else {
            "Maaf terjadi kesalahan saa memverifikasi email.";
        }
    }

}
