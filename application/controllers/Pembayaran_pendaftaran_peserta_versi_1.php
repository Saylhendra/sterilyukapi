<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_pendaftaran_peserta_versi_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Pembayaran_pendaftaran_peserta_model', '', TRUE);
        $this->load->model('Jadwal_model', '', TRUE);
        $this->load->model('Kru_model', '', TRUE);
        $this->load->model('Book_model', '', TRUE);
        $this->load->model('To_Book_model', '', TRUE);
        $this->load->model('Jadwal_block_user_model', '', TRUE);

    }

    public function index()
    {
        echo "Access Denied";
    }

    function addpembayaran_pendaftaran_peserta()
    {

        $id_jadwal = $this->input->post('id_jadwal');
        $id_user = $this->input->post('id_user');
        $id_book = $this->input->post('id_book');
        $id_bank_tujuan_pembayaran = $this->input->post('id_bank_tujuan_pembayaran');
        $bank_yang_digunakan = $this->input->post('bank_yang_digunakan');
        $nomor_rekening_bank_yang_digunakan = $this->input->post('nomor_rekening_bank_yang_digunakan');
        $nama_pemilik_nomor_rekening_bank_yang_digunakan = $this->input->post('nama_pemilik_nomor_rekening_bank_yang_digunakan');
        $tanggal_transfer = $this->input->post('tanggal_transfer');
        $keterangan = $this->input->post('keterangan');
        $foto_bukti_pembayaran = $this->input->post('foto_bukti_pembayaran');

        $response['isSuccess'] = false;
        $response['message'] = "Error";

        if ($foto_bukti_pembayaran == null) {
            $url_foto_bukti_pembayaran = "";
        } else {

            $image = base64_decode(str_replace('data:image/jpg;base64,', '', $foto_bukti_pembayaran));
            $upload_path = APPPATH . '../source/upload/image/foto_bukti_pembayaran/';
            $new_name = time();
            $file_name = $new_name;

            if (file_put_contents($upload_path . $file_name . ".jpg", $image)) {
                $url_foto_bukti_pembayaran = "/source/upload/image/foto_bukti_pembayaran/" . $file_name . ".jpg";
            } else {
                echo json_encode($response);
                return;
            }


        }

        if ($id_jadwal!= null
            && $id_user != null
            && $id_book != null
            && $id_bank_tujuan_pembayaran != null
            && $bank_yang_digunakan != null
            && $nomor_rekening_bank_yang_digunakan != null
            && $nama_pemilik_nomor_rekening_bank_yang_digunakan != null
            && $tanggal_transfer != null
        ) {

            $data = array(
                'id_book' => $id_book,
                'id_bank_tujuan_pembayaran' => $id_bank_tujuan_pembayaran,
                'bank_yang_digunakan' => $bank_yang_digunakan,
                'nomor_rekening_bank_yang_digunakan' => $nomor_rekening_bank_yang_digunakan,
                'nama_pemilik_nomor_rekening_bank_yang_digunakan' => $nama_pemilik_nomor_rekening_bank_yang_digunakan,
                'tanggal_transfer' => $tanggal_transfer,
                'keterangan' => $keterangan,
                'foto_bukti_pembayaran' => $url_foto_bukti_pembayaran,

            );

            $action_pembayaran_pendaftaran_peserta = $this->Pembayaran_pendaftaran_peserta_model->insertpembayaran_pendaftaran_peserta($data);
            if ($action_pembayaran_pendaftaran_peserta) {
                $response['isSuccess'] = true;
                $response['message'] = "konfirmasi pembayaran telah dikirim";

                $this->Book_model->update_status_book($id_user, $id_jadwal, $id_book,"3");

                $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_user);
                $kru_jadwal = $this->Kru_model->get_kru($id_jadwal, 1, 3);
                $book_jadwal = $this->Book_model->get_book("3", $id_jadwal, 1, 3);

                $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
                $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
                $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
                $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
                $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
                $response['biaya_steril_jantan_lokal'] = $detail_jadwal["biaya_steril_jantan_lokal"];
                $response['biaya_steril_jantan_mix'] = $detail_jadwal["biaya_steril_jantan_mix"];
                $response['biaya_steril_betina_lokal'] = $detail_jadwal["biaya_steril_betina_lokal"];
                $response['biaya_steril_betina_mix'] = $detail_jadwal["biaya_steril_betina_mix"];
                $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
                $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
                $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
                $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
                $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
                $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
                $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
                $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
                $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
                $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
                $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
                $response['nama_tipe_jadwal'] = $detail_jadwal["nama_tipe_jadwal"];
                $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
                $response['alamat_lokasi_jadwal'] = $detail_jadwal["alamat_lokasi_jadwal"];
                $response['rute_lokasi_jadwal'] = $detail_jadwal["rute_lokasi_jadwal"];
                $response['no_telp_lokasi_jadwal'] = $detail_jadwal["no_telp_lokasi_jadwal"];
                $response['latitude_lokasi_jadwal'] = $detail_jadwal["latitude_lokasi_jadwal"];
                $response['longitude_lokasi_jadwal'] = $detail_jadwal["longitude_lokasi_jadwal"];
                $response['id_status_jadwal'] = $detail_jadwal["id_status_jadwal"];
                $response['nama_status_jadwal'] = $detail_jadwal["nama_status_jadwal"];
                $response['id_user'] = $detail_jadwal["id_user"];
                $response['id_book'] = $detail_jadwal["id_book"];
                $response['kode_book'] = $detail_jadwal["kode_book"];
                $response['id_status_book'] = $detail_jadwal["id_status_book"];
                $response['biaya_book'] = $detail_jadwal["biaya_book"];
                $response['kuota_jantan_book'] = $detail_jadwal["kuota_jantan_book"];
                $response['kuota_jantan_lokal_book'] = $detail_jadwal["kuota_jantan_lokal_book"];
                $response['kuota_jantan_mix_book'] = $detail_jadwal["kuota_jantan_mix_book"];
                $response['kuota_betina_book'] = $detail_jadwal["kuota_betina_book"];
                $response['kuota_betina_lokal_book'] = $detail_jadwal["kuota_betina_lokal_book"];
                $response['kuota_betina_mix_book'] = $detail_jadwal["kuota_betina_mix_book"];
                $response['waktu_book'] = $detail_jadwal["waktu_book"];
                $response['nama_status_book'] = $detail_jadwal["nama_status_book"];
                $response['id_pembayaran_pendaftaran_peserta'] = $detail_jadwal["id_pembayaran_pendaftaran_peserta"];
                $response['id_bank_tujuan_pembayaran'] = $detail_jadwal["id_bank_tujuan_pembayaran"];
                $response['bank_yang_digunakan'] = $detail_jadwal["bank_yang_digunakan"];
                $response['nomor_rekening_bank_yang_digunakan'] = $detail_jadwal["nomor_rekening_bank_yang_digunakan"];
                $response['nama_pemilik_nomor_rekening_bank_yang_digunakan'] = $detail_jadwal["nama_pemilik_nomor_rekening_bank_yang_digunakan"];
                $response['tanggal_transfer'] = $detail_jadwal["tanggal_transfer"];
                $response['keterangan'] = $detail_jadwal["keterangan"];
                $response['waktu_pembayaran_pendaftaran_peserta'] = $detail_jadwal["waktu_pembayaran_pendaftaran_peserta"];
                $response['foto_bukti_pembayaran'] = $detail_jadwal["foto_bukti_pembayaran"];
                $response['id_status_pembayaran_pendaftaran_peserta'] = $detail_jadwal["id_status_pembayaran_pendaftaran_peserta"];
                $response['nama_status_pembayaran_pendaftaran_peserta'] = $detail_jadwal["nama_status_pembayaran_pendaftaran_peserta"];
                $response['nama_bank_tujuan_pembayaran'] = $detail_jadwal["nama_status_book"];
                $response['nomor_rekening_bank_tujuan_pembayaran'] = $detail_jadwal["nomor_rekening_bank_tujuan_pembayaran"];
                $response['atas_nama_bank_tujuan_pembayaran'] = $detail_jadwal["atas_nama_bank_tujuan_pembayaran"];
                $response['isToBook'] = $this->To_Book_model->check_exitst_by_id_jadwal_id_user($id_jadwal,$id_user);
                $response['is_blocked_user'] = $this->Jadwal_block_user_model->check_exitst_by_id_jadwal_id_user($id_jadwal,$id_user);
                $response['kru'] = $kru_jadwal;
                $response['book'] = $book_jadwal;
            } else {
                $response['message'] = "konfirmasi pembayaran gagal";
            }
        }
        echo json_encode($response);
    }


}
