<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_block_user_versi_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Pembayaran_pendaftaran_peserta_model', '', TRUE);


    }

    public function index()
    {
        echo "Access Denied";
    }

    function all_jadwal_block_user()
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['jadwal_block_user'] = $this->Jadwal_block_user_model->get_all_jadwal_block_user();
        echo json_encode($response);
    }

    function jadwal_block_user($id_jadwal,$page = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['jadwal_block_user'] = $this->Jadwal_block_user_model->get_jadwal_block_user($id_jadwal,$page);
        echo json_encode($response);
    }


    function addeditjadwal_block_user()
    {
        $id_jadwal_block_user = $this->input->post('id_jadwal_block_user');
        $id_jadwal = $this->input->post('id_jadwal');
        $id_user = $this->input->post('id_user');
        $keterangan_block = $this->input->post('keterangan_block');
        $response['isSuccess'] = false;
        $response['message'] = "Error";

        $jadwal_block_user = array(
            'id_jadwal' => $id_jadwal,
            'id_user' => $id_user,
            'keterangan_block' => $keterangan_block

        );

        if ($id_jadwal != null || $id_user != null) {

            $nb = $this->Jadwal_block_user_model->cekAccessJadwalBlockUser($id_jadwal, $id_user);

            if ($id_jadwal_block_user != null) {
                //edit
                if ($nb != null) {
                    $action_jadwal_block_user = $this->Jadwal_block_user_model->updatejadwal_block_user($id_jadwal_block_user, $jadwal_block_user);
                    if ($action_jadwal_block_user) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil mengedit jadwal_block_user";
                    } else {
                        $response['message'] = "gagal mengedit jadwal_block_user";
                    }
                } else {
                    $response['message'] = "gagal mengedit jadwal_block_user";
                }

            } else {
                $response['message'] = "gagal mengedit jadwal_block_user";
            }


        } else {
            $action_jadwal_block_user = $this->Jadwal_block_user_model->insertjadwal_block_user($jadwal_block_user);
            if ($action_jadwal_block_user) {
                $response['isSuccess'] = true;
                $response['message'] = "berhasil menambah jadwal_block_user";

            } else {
                $response['message'] = "gagal menambah jadwal_block_user";
            }
        }

        echo json_encode($response);
    }

    function delete_jadwal_block_user($id)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil menghapus jadwal_block_user";
        $this->Jadwal_block_user_model->delete_jadwal_block_user($id);
        echo json_encode($response);
    }


}
