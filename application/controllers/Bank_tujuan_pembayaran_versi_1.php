<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_tujuan_pembayaran_versi_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Bank_tujuan_pembayaran_model', '', TRUE);

    }

    public function index()
    {
        echo "Access Denied";
    }

    function all_bank_tujuan_pembayaran()
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['bank_tujuan_pembayaran'] = $this->Bank_tujuan_pembayaran_model->get_all_bank_tujuan_pembayaran();
        echo json_encode($response);
    }

}
