<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_versi_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Faq_model', '', TRUE);

    }

    public function index()
    {
        echo "Access Denied";
    }

    function all_faq()
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['faq'] = $this->Faq_model->get_all_faq();
        echo json_encode($response);
    }

    function faq($page = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['faq'] = $this->Faq_model->get_faq($page);
        echo json_encode($response);
    }

    function detail_faq($id_faq)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_faq = $this->Faq_model->getFaqById($id_faq);
        if ($detail_faq == null) {
            $response['isSuccess'] = false;
            $response['message'] = "not available";
        }
        $response['id_faq'] = $detail_faq["id_faq"];
        $response['question_faq'] = $detail_faq["question_faq"];
        $response['answer_faq'] = $detail_faq["answer_faq"];
        echo json_encode($response);
    }

    function addeditfaq()
    {
        $id_faq = $this->input->post('id_faq');
        $question_faq = $this->input->post('question_faq');
        $answer_faq = $this->input->post('answer_faq');
        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($question_faq != null || $answer_faq != null) {
            $faq = array(
                'question_faq' => $question_faq,
                'answer_faq' => $answer_faq,

            );

            $nb = $this->Faq_model->cekquestionfaq($question_faq);

            if ($id_faq != null) {
                if (strtolower($nb["question_faq"]) == strtolower($question_faq) || $nb == null) {
                    $action_faq = $this->Faq_model->updatefaq($id_faq, $faq);
                    if ($action_faq) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil mengedit faq";
                    } else {
                        $response['message'] = "gagal mengedit faq";
                    }
                } else {
                    $response['message'] = "Pertanyaan sudah ada...";
                }

            } else {
                if ($nb != null) {
                    $response['message'] = "Pertanyaan sudah ada...";
                } else {
                    $action_faq = $this->Faq_model->insertfaq($faq);
                    if ($action_faq) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil menambah faq";
                    } else {
                        $response['message'] = "gagal menambah faq";
                    }
                }
            }
        }
        echo json_encode($response);
    }

    function delete_faq($id)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil menghapus faq";
        $this->Faq_model->delete_faq($id);
        echo json_encode($response);
    }




}
