<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lokasi_jadwal_model extends CI_Model
{


    public function insertlokasi_jadwal($lokasi_jadwal)
    {
        $query = $this->db->insert('lokasi_jadwal', $lokasi_jadwal);
        return $query;
    }

    public function updatelokasi_jadwal($id_lokasi_jadwal, $lokasi_jadwal)
    {
        $this->db->where('id_lokasi_jadwal', $id_lokasi_jadwal);
        $query = $this->db->update('lokasi_jadwal', $lokasi_jadwal);
        return $query;
    }


    public function ceknamalokasi_jadwal($nama_lokasi_jadwal)
    {
        $this->db
            ->select("*");
        $this->db->from('lokasi_jadwal');
        $this->db->where('nama_lokasi_jadwal', $nama_lokasi_jadwal);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function cekkodelokasi_jadwal($kode_lokasi_jadwal)
    {
        $this->db
            ->select("*");
        $this->db->from('lokasi_jadwal');
        $this->db->where('kode_lokasi_jadwal', $kode_lokasi_jadwal);
        $query = $this->db->get();

        return $query->row_array();
    }

    function get_lokasi_jadwal($page)
    {
        if ($page == null || $page == 1) {
            $page = 1;
        }

        $limit = "10";
        $start = ($page - 1) * $limit;
        $this->db
            ->select("lokasi_jadwal.*");
        $this->db->from('lokasi_jadwal');
        $this->db->order_by('lokasi_jadwal.id_lokasi_jadwal', 'DESC');
        $this->db->limit($limit,$start);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_lokasi_jadwal()
    {
        $this->db
            ->select("*");
        $this->db->from('lokasi_jadwal');
        $this->db->order_by('lokasi_jadwal.nama_lokasi_jadwal', 'ASC');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getLokasiJadwalById($id_lokasi_jadwal)
    {
        $this->db
            ->select("*");
        $this->db->from('lokasi_jadwal');
        $this->db->where('id_lokasi_jadwal', $id_lokasi_jadwal);
        $query = $this->db->get();

        return $query->row_array();
    }


    public function getLokasiJadwalByKodeLokasiJadwal($kode_lokasi_jadwal)
    {
        $this->db
            ->select("*");
        $this->db->from('lokasi_jadwal');
        $this->db->where('kode_lokasi_jadwal', $kode_lokasi_jadwal);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function tambah_kuantiti_lokasi_jadwal($ex_id_lokasi_jadwal, $ex_kuantiti_detail_transaksi)
    {
        $this->db->where('id_lokasi_jadwal', $ex_id_lokasi_jadwal);
        $this->db->set('kuantiti_saat_ini', 'kuantiti_saat_ini+' . $ex_kuantiti_detail_transaksi, FALSE);
        $query = $this->db->update('lokasi_jadwal');
        return $query;
    }

    public function kurang_kuantiti_lokasi_jadwal($ex_id_lokasi_jadwal, $ex_kuantiti_detail_transaksi)
    {
        $this->db->where('id_lokasi_jadwal', $ex_id_lokasi_jadwal);
        $this->db->set('kuantiti_saat_ini', 'kuantiti_saat_ini-' . $ex_kuantiti_detail_transaksi, FALSE);
        $query = $this->db->update('lokasi_jadwal');
        return $query;
    }


    public function getCountLokasiJadwalByKategori($kategori)
    {
        $this->db
            ->select("*");
        $this->db->from('lokasi_jadwal');
        if ($kategori == "1") {
            $this->db->where(" lokasi_jadwal.kuantiti_saat_ini ='0' ");
        } else if ($kategori == "2") {
            $this->db->where(" lokasi_jadwal.kuantiti_saat_ini >'0' && lokasi_jadwal.kuantiti_saat_ini <='5' ");
        } else if ($kategori == "3") {

            $this->db->where(" lokasi_jadwal.kuantiti_saat_ini > '5' ");
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function delete_lokasi_jadwal($id)
    {
        $this->db->where('id_lokasi_jadwal', $id);
        $this->db->delete('lokasi_jadwal');
    }

}