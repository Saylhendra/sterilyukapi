<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class To_Book_model extends CI_Model
{


    public function insertto_book($to_book)
    {
        $query = $this->db->insert('to_book', $to_book);
        return $query;
    }

    public function updateto_book($id_to_book, $to_book)
    {
        $this->db->where('id_to_book', $id_to_book);
        $query = $this->db->update('to_book', $to_book);
        return $query;
    }


    function delete_to_book_by_id($id)
    {
        $this->db->where('id_to_book', $id);
        $this->db->delete('to_book');
    }

    function delete_to_book_by_id_jadwal_id_user($id_jadwal,$id_user)
    {
        $this->db->where('id_jadwal', $id_jadwal);
        $this->db->where('id_user', $id_user);
        $query= $this->db->delete('to_book');
        return $query;
    }

    function check_exitst_by_id_jadwal_id_user($id_jadwal,$id_user)
    {
        $this->db->where('id_jadwal', $id_jadwal);
        $this->db->where('id_user', $id_user);
        $query = $this->db->get('to_book');
        if ($query->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }

    }
}