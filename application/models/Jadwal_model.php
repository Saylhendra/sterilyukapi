<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Jadwal_model $Jadwal_model
 */

class Jadwal_model extends CI_Model
{

    public function insertjadwal($jadwal)
    {
        $query = $this->db->insert('jadwal', $jadwal);

        return $query;
    }

    public function updatejadwal($id_jadwal, $jadwal)
    {
        $this->db->where('id_jadwal', $id_jadwal);
        $query = $this->db->update('jadwal', $jadwal);
        return $query;
    }

    public function getJadwalByKodeJadwal($kode_jadwal)
    {
        $this->db
            ->select("*");
        $this->db->from('jadwal');
        $this->db->where('kode_jadwal', $kode_jadwal);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function getNewKodeJadwal()
    {
        $this->db
            ->select("*");
        $this->db->from('jadwal');
        $this->db->order_by("id_jadwal", "desc");
        $this->db->limit(1, 0);
        $query = $this->db->get();

        return $query->row_array();
    }


    function get_jadwal($kode_tipe_jadwal, $page, $id_user = null)
    {
        if ($id_user == null) {
            $id_user = "0";
        }
        if ($page == null || $page == 1) {
            $page = 1;
        }

        $limit = "10";
        $start = ($page - 1) * $limit;

        $this->db
            ->select("jadwal.id_jadwal as idnya,
            jadwal.*,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            status_jadwal.nama_status_jadwal,
            lokasi_jadwal.nama_lokasi_jadwal,
            lokasi_jadwal.alamat_lokasi_jadwal,
            lokasi_jadwal.rute_lokasi_jadwal,
            lokasi_jadwal.no_telp_lokasi_jadwal,
            lokasi_jadwal.latitude_lokasi_jadwal,
            lokasi_jadwal.longitude_lokasi_jadwal,
            (SELECT id_user FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_user,
            (SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_book,
            (SELECT kode_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kode_book,
            (SELECT id_status_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_status_book,
            (SELECT biaya_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS biaya_book,
            (SELECT kuota_jantan_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_book,
            (SELECT kuota_jantan_lokal_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_lokal_book,
            (SELECT kuota_jantan_mix_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_mix_book,
            (SELECT kuota_betina_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_book,
            (SELECT kuota_betina_lokal_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_lokal_book,
            (SELECT kuota_betina_mix_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_mix_book,
            (SELECT waktu_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS waktu_book,
            (SELECT nama_status_book FROM book INNER JOIN status_book ON (book.id_status_book = status_book.id_status_book) WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS nama_status_book,,
            (SELECT id_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_pembayaran_pendaftaran_peserta,
            (SELECT id_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_bank_tujuan_pembayaran,
            (SELECT bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS bank_yang_digunakan,
            (SELECT nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_yang_digunakan,
            (SELECT nama_pemilik_nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_pemilik_nomor_rekening_bank_yang_digunakan,
            (SELECT tanggal_transfer FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS tanggal_transfer,
            (SELECT keterangan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS keterangan,
            (SELECT waktu_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS waktu_pembayaran_pendaftaran_peserta,
            (SELECT foto_bukti_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS foto_bukti_pembayaran,
            (SELECT id_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_bank_tujuan_pembayaran,
            (SELECT nomor_rekening_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_tujuan_pembayaran,
            (SELECT atas_nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS atas_nama_bank_tujuan_pembayaran,
            (SELECT count(id_jadwal_block_user) FROM jadwal_block_user  WHERE id_jadwal=idnya AND id_user='" . $id_user . "') AS is_blocked_user");

        $this->db->from('jadwal');

        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('lokasi_jadwal', 'lokasi_jadwal.id_lokasi_jadwal = jadwal.id_lokasi_jadwal');
        $this->db->join('status_jadwal', 'status_jadwal.id_status_jadwal = jadwal.id_status_jadwal');

        if ($kode_tipe_jadwal == "TIDAK_AKTIF") {
            $this->db->where('jadwal.id_status_aktif', "2");
        } else {
            $this->db->where('tipe_jadwal.kode_tipe_jadwal', $kode_tipe_jadwal);

            $this->db->where('jadwal.id_status_aktif', "1");
        }

        $this->db->order_by('jadwal.waktu_mulai_jadwal', 'DESC');
        $this->db->limit($limit, $start);

        $query = $this->db->get();


        return $query->result_array();
    }

    function my_book($page, $id_user)
    {
        if ($page == null || $page == 1) {
            $page = 1;
        }

        $limit = "10";
        $start = ($page - 1) * $limit;

        $this->db
            ->select("
            jadwal.*,
            jadwal.id_jadwal as idnya,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            status_jadwal.nama_status_jadwal,
            lokasi_jadwal.nama_lokasi_jadwal,
            lokasi_jadwal.alamat_lokasi_jadwal,
            lokasi_jadwal.rute_lokasi_jadwal,
            lokasi_jadwal.no_telp_lokasi_jadwal,
            lokasi_jadwal.latitude_lokasi_jadwal,
            lokasi_jadwal.longitude_lokasi_jadwal,
            t1.*,
            status_book.nama_status_book,
            (SELECT count(id_jadwal_block_user) FROM jadwal_block_user  WHERE id_jadwal=idnya AND id_user='" . $id_user . "') AS is_blocked_user");
        $this->db->from('book t1');
        $this->db->join("(SELECT id_jadwal, MAX(id_book) id_book FROM book WHERE id_user='" . $id_user . "' GROUP BY id_jadwal) t2", "t1.id_book = t2.id_book AND t1.id_jadwal = t2.id_jadwal");
        $this->db->join('jadwal', 't1.id_jadwal = jadwal.id_jadwal');
        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('lokasi_jadwal', 'lokasi_jadwal.id_lokasi_jadwal = jadwal.id_lokasi_jadwal');
        $this->db->join('status_book', 'status_book.id_status_book = t1.id_status_book');
        $this->db->join('status_jadwal', 'status_jadwal.id_status_jadwal = jadwal.id_status_jadwal');
        $this->db->where('t1.id_status_book !=', "5");
        $this->db->where('jadwal.id_status_aktif', "1");
        $this->db->order_by('jadwal.waktu_mulai_jadwal', 'ASC');
        $this->db->limit($limit, $start);

        $query = $this->db->get();


        return $query->result_array();
    }

    public function my_record_book($page, $id_user)
    {
        if ($page == null || $page == 1) {
            $page = 1;
        }

        $limit = "10";
        $start = ($page - 1) * $limit;

        $this->db
            ->select("
            jadwal.*,
            jadwal.id_jadwal as idnya,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            status_jadwal.nama_status_jadwal,
            lokasi_jadwal.nama_lokasi_jadwal,
            lokasi_jadwal.alamat_lokasi_jadwal,
            lokasi_jadwal.rute_lokasi_jadwal,
            lokasi_jadwal.no_telp_lokasi_jadwal,
            lokasi_jadwal.latitude_lokasi_jadwal,
            lokasi_jadwal.longitude_lokasi_jadwal,
            t1.*,
            status_book.nama_status_book,
            (SELECT count(id_jadwal_block_user) FROM jadwal_block_user  WHERE id_jadwal=idnya AND id_user='" . $id_user . "') AS is_blocked_user");
        $this->db->from('book t1');
        $this->db->join("(SELECT id_jadwal, MAX(id_book) id_book FROM book WHERE id_user='" . $id_user . "' GROUP BY id_jadwal) t2", "t1.id_book = t2.id_book AND t1.id_jadwal = t2.id_jadwal");
        $this->db->join('jadwal', 't1.id_jadwal = jadwal.id_jadwal');
        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('lokasi_jadwal', 'lokasi_jadwal.id_lokasi_jadwal = jadwal.id_lokasi_jadwal');
        $this->db->join('status_book', 'status_book.id_status_book = t1.id_status_book');
        $this->db->join('status_jadwal', 'status_jadwal.id_status_jadwal = jadwal.id_status_jadwal');
        $this->db->where('t1.id_status_book', "4");
        $this->db->where('jadwal.id_status_aktif', "1");
        $this->db->order_by('jadwal.waktu_mulai_jadwal', 'ASC');
        $this->db->limit($limit, $start);

        $query = $this->db->get();


        return $query->result_array();
    }

    function count_my_book($id_user)
    {
        $this->db
            ->select("t1.*");
        $this->db->from('book t1');
        $this->db->join("(SELECT id_jadwal, MAX(id_book) id_book FROM book WHERE id_user='" . $id_user . "' GROUP BY id_jadwal) t2", "t1.id_book = t2.id_book AND t1.id_jadwal = t2.id_jadwal");
        $this->db->join('jadwal', 't1.id_jadwal = jadwal.id_jadwal');
        $this->db->join('status_book', 'status_book.id_status_book = t1.id_status_book');
        $this->db->where('t1.id_status_book !=', "5");
        $this->db->where('t1.id_status_book !=', "6");
        $this->db->where('jadwal.id_status_aktif', "1");
        $this->db->order_by('jadwal.waktu_mulai_jadwal', 'ASC');

        $query = $this->db->get();
        return $query->num_rows();
    }

    function to_book($page, $id_user)
    {
        if ($page == null || $page == 1) {
            $page = 1;
        }

        $limit = "10";
        $start = ($page - 1) * $limit;

        $this->db
            ->select("to_book.id_jadwal as idnya,
            jadwal.*,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            status_jadwal.nama_status_jadwal,
            lokasi_jadwal.nama_lokasi_jadwal,
            lokasi_jadwal.alamat_lokasi_jadwal,
            lokasi_jadwal.rute_lokasi_jadwal,
            lokasi_jadwal.no_telp_lokasi_jadwal,
            lokasi_jadwal.latitude_lokasi_jadwal,
            lokasi_jadwal.longitude_lokasi_jadwal,
            (SELECT id_user FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_user,
            (SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_book,
            (SELECT kode_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kode_book,
            (SELECT id_status_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_status_book,
            (SELECT biaya_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS biaya_book,
            (SELECT kuota_jantan_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_book,
            (SELECT kuota_jantan_lokal_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_lokal_book,
            (SELECT kuota_jantan_mix_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_mix_book,
            (SELECT kuota_betina_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_book,
            (SELECT kuota_betina_lokal_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_lokal_book,
            (SELECT kuota_betina_mix_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_mix_book,
            (SELECT waktu_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS waktu_book,
            (SELECT nama_status_book FROM book INNER JOIN status_book ON (book.id_status_book = status_book.id_status_book) WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS nama_status_book,,
            (SELECT id_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_pembayaran_pendaftaran_peserta,
            (SELECT id_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_bank_tujuan_pembayaran,
            (SELECT bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS bank_yang_digunakan,
            (SELECT nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_yang_digunakan,
            (SELECT nama_pemilik_nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_pemilik_nomor_rekening_bank_yang_digunakan,
            (SELECT tanggal_transfer FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS tanggal_transfer,
            (SELECT keterangan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS keterangan,
            (SELECT waktu_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS waktu_pembayaran_pendaftaran_peserta,
            (SELECT foto_bukti_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS foto_bukti_pembayaran,
            (SELECT id_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_bank_tujuan_pembayaran,
            (SELECT nomor_rekening_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_tujuan_pembayaran,
            (SELECT atas_nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS atas_nama_bank_tujuan_pembayaran,
            (SELECT count(id_jadwal_block_user) FROM jadwal_block_user  WHERE id_jadwal=idnya AND id_user='" . $id_user . "') AS is_blocked_user");
        $this->db->from('to_book');
        $this->db->join('jadwal', 'to_book.id_jadwal = jadwal.id_jadwal');
        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('lokasi_jadwal', 'lokasi_jadwal.id_lokasi_jadwal = jadwal.id_lokasi_jadwal');
        $this->db->join('status_jadwal', 'status_jadwal.id_status_jadwal = jadwal.id_status_jadwal');

        $this->db->where('to_book.id_user', $id_user);

        $this->db->order_by('to_book.id_to_book', 'DESC');
        $this->db->limit($limit, $start);

        $query = $this->db->get();


        return $query->result_array();
    }


    public function getJadwalById($id_jadwal, $id_user = null)
    {
        if ($id_user == null) {
            $id_user = "0";
        }
        $this->db
            ->select("jadwal.id_jadwal as idnya,
            jadwal.*,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            status_jadwal.nama_status_jadwal,
            lokasi_jadwal.nama_lokasi_jadwal,
            lokasi_jadwal.alamat_lokasi_jadwal,
            lokasi_jadwal.rute_lokasi_jadwal,
            lokasi_jadwal.no_telp_lokasi_jadwal,
            lokasi_jadwal.latitude_lokasi_jadwal,
            lokasi_jadwal.longitude_lokasi_jadwal,
            (SELECT id_user FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_user,
            (SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_book,
            (SELECT kode_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kode_book,
            (SELECT id_status_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS id_status_book,
            (SELECT biaya_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS biaya_book,
            (SELECT kuota_jantan_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_book,
            (SELECT kuota_jantan_lokal_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_lokal_book,
            (SELECT kuota_jantan_mix_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_jantan_mix_book,
            (SELECT kuota_betina_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_book,
            (SELECT kuota_betina_lokal_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_lokal_book,
            (SELECT kuota_betina_mix_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS kuota_betina_mix_book,
            (SELECT waktu_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS waktu_book,
            (SELECT nama_status_book FROM book INNER JOIN status_book ON (book.id_status_book = status_book.id_status_book) WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1) AS nama_status_book,
            (SELECT id_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_pembayaran_pendaftaran_peserta,
            (SELECT id_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_bank_tujuan_pembayaran,
            (SELECT bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS bank_yang_digunakan,
            (SELECT nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_yang_digunakan,
            (SELECT nama_pemilik_nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_pemilik_nomor_rekening_bank_yang_digunakan,
            (SELECT tanggal_transfer FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS tanggal_transfer,
            (SELECT keterangan FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS keterangan,
            (SELECT waktu_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS waktu_pembayaran_pendaftaran_peserta,
            (SELECT foto_bukti_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS foto_bukti_pembayaran,
            (SELECT id_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_bank_tujuan_pembayaran,
            (SELECT nomor_rekening_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_tujuan_pembayaran,
            (SELECT atas_nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=(SELECT id_book FROM book WHERE id_jadwal=idnya AND id_user='" . $id_user . "'  ORDER BY id_book DESC LIMIT 0,1)   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS atas_nama_bank_tujuan_pembayaran,
            (SELECT count(id_jadwal_block_user) FROM jadwal_block_user  WHERE id_jadwal=idnya AND id_user='" . $id_user . "') AS is_blocked_user");

        $this->db->from('jadwal');

        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('lokasi_jadwal', 'lokasi_jadwal.id_lokasi_jadwal = jadwal.id_lokasi_jadwal');
        $this->db->join('status_jadwal', 'status_jadwal.id_status_jadwal = jadwal.id_status_jadwal');

        $this->db->where('jadwal.id_jadwal', $id_jadwal);

        $query = $this->db->get();

        return $query->row_array();
    }

    function count_jadwal($id_user = null, $kode_tipe_jadwal = null)
    {

        $this->db
            ->select("jadwal.id_jadwal as idnya,
            jadwal.*,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            user.nama,
            (SELECT COUNT(id_detail_jadwal) AS count_item FROM detail_jadwal where id_jadwal=idnya)AS count_item");

        $this->db->from('jadwal');

        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('user', 'user.id_user = jadwal.id_user');

        if ($id_user != null) {
            $this->db->where('jadwal.id_user', $id_user);
        }
        if ($kode_tipe_jadwal != null) {
            $this->db->where('tipe_jadwal.kode_tipe_jadwal', $kode_tipe_jadwal);

        }

        $query = $this->db->get();

        return $query->num_rows();
    }

}