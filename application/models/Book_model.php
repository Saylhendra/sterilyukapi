<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Book_model extends CI_Model
{


    public function insertbook($book)
    {
        $query = $this->db->insert('book', $book);
        return $query;
    }

    public function updatebook($id_book, $book)
    {
        $this->db->where('id_book', $id_book);
        $query = $this->db->update('book', $book);
        return $query;
    }

    public function getNewKodeBook()
    {
        $this->db
            ->select("*");
        $this->db->from('book');
        $this->db->order_by("id_book", "desc");
        $this->db->limit(1, 0);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function cekAccessBook($id_jadwal, $id_user)
    {
        $this->db
            ->select("*");
        $this->db->from('book');
        $this->db->where('id_jadwal', $id_jadwal);
        $this->db->where('id_user', $id_user);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function cekkodebook($kode_book)
    {
        $this->db
            ->select("*");
        $this->db->from('book');
        $this->db->where('kode_book', $kode_book);
        $query = $this->db->get();

        return $query->row_array();
    }


    function get_book($id_level_status = null, $id_jadwal, $page, $limit = null)
    {

        if ($limit == null) {
            $limit = "10";
        }

        $this->db
            ->select("
                    book.id_book as idnya,
                    book.*
                    , user.nama_lengkap
                    , user.alamat
                    , user.foto_profil
                    , status_book.nama_status_book
           , (SELECT id_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_pembayaran_pendaftaran_peserta,
            (SELECT id_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_bank_tujuan_pembayaran,
            (SELECT bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS bank_yang_digunakan,
            (SELECT nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_yang_digunakan,
            (SELECT nama_pemilik_nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_pemilik_nomor_rekening_bank_yang_digunakan,
            (SELECT tanggal_transfer FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS tanggal_transfer,
            (SELECT keterangan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS keterangan,
            (SELECT waktu_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS waktu_pembayaran_pendaftaran_peserta,
            (SELECT foto_bukti_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS foto_bukti_pembayaran,
            (SELECT id_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_bank_tujuan_pembayaran,
            (SELECT nomor_rekening_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_tujuan_pembayaran,
            (SELECT atas_nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS atas_nama_bank_tujuan_pembayaran");

        $this->db->from('book');

        $this->db->join('status_book', 'book.id_status_book = status_book.id_status_book');
        $this->db->join('user', 'book.id_user = user.id_user');

        $this->db->where('book.id_jadwal', $id_jadwal);

        if ($id_level_status != "1") {
            $this->db->where('book.id_status_book', "4");
        }


        $this->db->order_by('book.id_book', 'DESC');
        $this->db->limit($limit, $page - 1);
        $query = $this->db->get();

        return $query->result_array();
    }


    function get_manage_book($id_jadwal, $id_status_book, $page)
    {
        if ($page == null || $page == 1) {
            $page = 1;
        }

        $limit = "10";
        $start = ($page - 1) * $limit;

        $this->db
            ->select("
                    book.id_book as idnya,
                    book.*
                    , user.nama_lengkap
                    , user.alamat
                    , user.foto_profil
                    , status_book.nama_status_book
           , (SELECT id_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_pembayaran_pendaftaran_peserta,
            (SELECT id_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_bank_tujuan_pembayaran,
            (SELECT bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS bank_yang_digunakan,
            (SELECT nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_yang_digunakan,
            (SELECT nama_pemilik_nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_pemilik_nomor_rekening_bank_yang_digunakan,
            (SELECT tanggal_transfer FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS tanggal_transfer,
            (SELECT keterangan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS keterangan,
            (SELECT waktu_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS waktu_pembayaran_pendaftaran_peserta,
            (SELECT foto_bukti_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS foto_bukti_pembayaran,
            (SELECT id_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_bank_tujuan_pembayaran,
            (SELECT nomor_rekening_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_tujuan_pembayaran,
            (SELECT atas_nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS atas_nama_bank_tujuan_pembayaran");
        $this->db->from('book');

        $this->db->join('status_book', 'book.id_status_book = status_book.id_status_book');
        $this->db->join('user', 'book.id_user = user.id_user');

        $this->db->where('book.id_jadwal', $id_jadwal);

        $this->db->where('book.id_status_book', $id_status_book);


        $this->db->order_by('book.id_book', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        return $query->result_array();
    }

    function get_book_id_book($id_book)
    {
        $this->db
            ->select("
                    book.id_book as idnya,
                    book.*
                    , user.nama_lengkap
                    , user.alamat
                    , user.foto_profil
                    , status_book.nama_status_book
           , (SELECT id_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_pembayaran_pendaftaran_peserta,
            (SELECT id_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_bank_tujuan_pembayaran,
            (SELECT bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS bank_yang_digunakan,
            (SELECT nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_yang_digunakan,
            (SELECT nama_pemilik_nomor_rekening_bank_yang_digunakan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_pemilik_nomor_rekening_bank_yang_digunakan,
            (SELECT tanggal_transfer FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS tanggal_transfer,
            (SELECT keterangan FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS keterangan,
            (SELECT waktu_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS waktu_pembayaran_pendaftaran_peserta,
            (SELECT foto_bukti_pembayaran FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS foto_bukti_pembayaran,
            (SELECT id_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS id_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_status_pembayaran_pendaftaran_peserta FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_status_pembayaran_pendaftaran_peserta,
            (SELECT nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nama_bank_tujuan_pembayaran,
            (SELECT nomor_rekening_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS nomor_rekening_bank_tujuan_pembayaran,
            (SELECT atas_nama_bank_tujuan_pembayaran FROM pembayaran_pendaftaran_peserta INNER JOIN status_pembayaran_pendaftaran_peserta ON (pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta = status_pembayaran_pendaftaran_peserta.id_status_pembayaran_pendaftaran_peserta)
             INNER JOIN bank_tujuan_pembayaran ON (pembayaran_pendaftaran_peserta.id_bank_tujuan_pembayaran = bank_tujuan_pembayaran.id_bank_tujuan_pembayaran) WHERE id_book=idnya   ORDER BY id_pembayaran_pendaftaran_peserta  DESC LIMIT 0,1) AS atas_nama_bank_tujuan_pembayaran");
        $this->db->from('book');

        $this->db->join('status_book', 'book.id_status_book = status_book.id_status_book');
        $this->db->join('user', 'book.id_user = user.id_user');

        $this->db->where('book.id_book', $id_book);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function get_all_book()
    {
        $this->db
            ->select("*");
        $this->db->from('book');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getBookById($id_book)
    {
        $this->db
            ->select("*");
        $this->db->from('book');
        $this->db->where('id_book', $id_book);
        $query = $this->db->get();

        return $query->row_array();
    }


    public function getBookByKodeBook($kode_book)
    {
        $this->db
            ->select("*");
        $this->db->from('book');
        $this->db->where('kode_book', $kode_book);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function tambah_kuantiti_book($id_book, $kuantiti_detail_transaksi)
    {
        $this->db->where('id_book', $id_book);
        $this->db->set('kuantiti_saat_ini', 'kuantiti_saat_ini+' . $kuantiti_detail_transaksi, FALSE);
        $query = $this->db->update('book');
        return $query;
    }

    public function kurang_kuantiti_book($ex_id_book, $ex_kuantiti_detail_transaksi)
    {
        $this->db->where('id_book', $ex_id_book);
        $this->db->set('kuantiti_saat_ini', 'kuantiti_saat_ini-' . $ex_kuantiti_detail_transaksi, FALSE);
        $query = $this->db->update('book');
        return $query;
    }


    public function getCountBookByKategori($kategori)
    {
        $this->db
            ->select("*");
        $this->db->from('book');
        if ($kategori == "1") {
            $this->db->where(" book.kuantiti_saat_ini ='0' ");
        } else if ($kategori == "2") {
            $this->db->where(" book.kuantiti_saat_ini >'0' && book.kuantiti_saat_ini <='5' ");
        } else if ($kategori == "3") {

            $this->db->where(" book.kuantiti_saat_ini > '5' ");
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function delete_book($id)
    {
        $this->db->where('id_book', $id);
        $this->db->delete('book');
    }


    function update_status_book($id_user, $id_jadwal, $id_book, $status_book)
    {
        $this->db->where('id_user', $id_user);
        $this->db->where('id_jadwal', $id_jadwal);
        $this->db->where('id_book', $id_book);
        $this->db->set('id_status_book', $status_book, FALSE);
        $query = $this->db->update('book');
        return $query;
    }


}