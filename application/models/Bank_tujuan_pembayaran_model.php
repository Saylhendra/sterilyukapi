<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bank_tujuan_pembayaran_model extends CI_Model
{


    public function insertbank_tujuan_pembayaran($bank_tujuan_pembayaran)
    {
        $query = $this->db->insert('bank_tujuan_pembayaran', $bank_tujuan_pembayaran);
        return $query;
    }

    public function updatebank_tujuan_pembayaran($id_bank_tujuan_pembayaran, $bank_tujuan_pembayaran)
    {
        $this->db->where('id_bank_tujuan_pembayaran', $id_bank_tujuan_pembayaran);
        $query = $this->db->update('bank_tujuan_pembayaran', $bank_tujuan_pembayaran);
        return $query;
    }


    public function cekquestionbank_tujuan_pembayaran($question_bank_tujuan_pembayaran)
    {
        $this->db
            ->select("*");
        $this->db->from('bank_tujuan_pembayaran');
        $this->db->where('question_bank_tujuan_pembayaran', $question_bank_tujuan_pembayaran);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function cekkodebank_tujuan_pembayaran($kode_bank_tujuan_pembayaran)
    {
        $this->db
            ->select("*");
        $this->db->from('bank_tujuan_pembayaran');
        $this->db->where('kode_bank_tujuan_pembayaran', $kode_bank_tujuan_pembayaran);
        $query = $this->db->get();

        return $query->row_array();
    }


    function get_bank_tujuan_pembayaran($page)
    {
        if ($page == null || $page == 1) {
            $page = 1;
        }

        $limit = "10";
        $start = ($page - 1) * $limit;
        $this->db
            ->select("bank_tujuan_pembayaran.*");
        $this->db->from('bank_tujuan_pembayaran');
        $this->db->order_by('bank_tujuan_pembayaran.id_bank_tujuan_pembayaran', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_bank_tujuan_pembayaran()
    {
        $this->db
            ->select("*");
        $this->db->from('bank_tujuan_pembayaran');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getBank_tujuan_pembayaranById($id_bank_tujuan_pembayaran)
    {
        $this->db
            ->select("*");
        $this->db->from('bank_tujuan_pembayaran');
        $this->db->where('id_bank_tujuan_pembayaran', $id_bank_tujuan_pembayaran);
        $query = $this->db->get();

        return $query->row_array();
    }


    public function getBank_tujuan_pembayaranByKodeBank_tujuan_pembayaran($kode_bank_tujuan_pembayaran)
    {
        $this->db
            ->select("*");
        $this->db->from('bank_tujuan_pembayaran');
        $this->db->where('kode_bank_tujuan_pembayaran', $kode_bank_tujuan_pembayaran);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function tambah_kuantiti_bank_tujuan_pembayaran($ex_id_bank_tujuan_pembayaran, $ex_kuantiti_detail_transaksi)
    {
        $this->db->where('id_bank_tujuan_pembayaran', $ex_id_bank_tujuan_pembayaran);
        $this->db->set('kuantiti_saat_ini', 'kuantiti_saat_ini+' . $ex_kuantiti_detail_transaksi, FALSE);
        $query = $this->db->update('bank_tujuan_pembayaran');
        return $query;
    }

    public function kurang_kuantiti_bank_tujuan_pembayaran($ex_id_bank_tujuan_pembayaran, $ex_kuantiti_detail_transaksi)
    {
        $this->db->where('id_bank_tujuan_pembayaran', $ex_id_bank_tujuan_pembayaran);
        $this->db->set('kuantiti_saat_ini', 'kuantiti_saat_ini-' . $ex_kuantiti_detail_transaksi, FALSE);
        $query = $this->db->update('bank_tujuan_pembayaran');
        return $query;
    }


    public function getCountBank_tujuan_pembayaranByKategori($kategori)
    {
        $this->db
            ->select("*");
        $this->db->from('bank_tujuan_pembayaran');
        if ($kategori == "1") {
            $this->db->where(" bank_tujuan_pembayaran.kuantiti_saat_ini ='0' ");
        } else if ($kategori == "2") {
            $this->db->where(" bank_tujuan_pembayaran.kuantiti_saat_ini >'0' && bank_tujuan_pembayaran.kuantiti_saat_ini <='5' ");
        } else if ($kategori == "3") {

            $this->db->where(" bank_tujuan_pembayaran.kuantiti_saat_ini > '5' ");
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function delete_bank_tujuan_pembayaran($id)
    {
        $this->db->where('id_bank_tujuan_pembayaran', $id);
        $this->db->delete('bank_tujuan_pembayaran');
    }

}