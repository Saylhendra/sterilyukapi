<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal_block_user_model extends CI_Model
{


    public function insertjadwal_block_user($jadwal_block_user)
    {
        $query = $this->db->insert('jadwal_block_user', $jadwal_block_user);
        return $query;
    }

    public function updatejadwal_block_user($id_jadwal_block_user, $jadwal_block_user)
    {
        $this->db->where('id_jadwal_block_user', $id_jadwal_block_user);
        $query = $this->db->update('jadwal_block_user', $jadwal_block_user);
        return $query;
    }


    function delete_jadwal_block_user_by_id($id)
    {
        $this->db->where('id_jadwal_block_user', $id);
        $this->db->delete('jadwal_block_user');
    }

    function delete_jadwal_block_user_by_id_jadwal_id_user($id_jadwal,$id_user)
    {
        $this->db->where('id_jadwal', $id_jadwal);
        $this->db->where('id_user', $id_user);
        $query= $this->db->delete('jadwal_block_user');
        return $query;
    }

    function check_exitst_by_id_jadwal_id_user($id_jadwal,$id_user)
    {
        $this->db->where('id_jadwal', $id_jadwal);
        $this->db->where('id_user', $id_user);
        $query = $this->db->get('jadwal_block_user');
        if ($query->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }

    }
}