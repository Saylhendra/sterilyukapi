<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Faq_model extends CI_Model
{


    public function insertfaq($faq)
    {
        $query = $this->db->insert('faq', $faq);
        return $query;
    }

    public function updatefaq($id_faq, $faq)
    {
        $this->db->where('id_faq', $id_faq);
        $query = $this->db->update('faq', $faq);
        return $query;
    }


    public function cekquestionfaq($question_faq)
    {
        $this->db
            ->select("*");
        $this->db->from('faq');
        $this->db->where('question_faq', $question_faq);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function cekkodefaq($kode_faq)
    {
        $this->db
            ->select("*");
        $this->db->from('faq');
        $this->db->where('kode_faq', $kode_faq);
        $query = $this->db->get();

        return $query->row_array();
    }


    function get_faq($page)
    {
        if ($page == null || $page == 1) {
            $page = 1;
        }

        $limit = "10";
        $start = ($page - 1) * $limit;
        $this->db
            ->select("faq.*");
        $this->db->from('faq');
        $this->db->order_by('faq.id_faq', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_faq()
    {
        $this->db
            ->select("*");
        $this->db->from('faq');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getFaqById($id_faq)
    {
        $this->db
            ->select("*");
        $this->db->from('faq');
        $this->db->where('id_faq', $id_faq);
        $query = $this->db->get();

        return $query->row_array();
    }


    public function getFaqByKodeFaq($kode_faq)
    {
        $this->db
            ->select("*");
        $this->db->from('faq');
        $this->db->where('kode_faq', $kode_faq);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function tambah_kuantiti_faq($ex_id_faq, $ex_kuantiti_detail_transaksi)
    {
        $this->db->where('id_faq', $ex_id_faq);
        $this->db->set('kuantiti_saat_ini', 'kuantiti_saat_ini+' . $ex_kuantiti_detail_transaksi, FALSE);
        $query = $this->db->update('faq');
        return $query;
    }

    public function kurang_kuantiti_faq($ex_id_faq, $ex_kuantiti_detail_transaksi)
    {
        $this->db->where('id_faq', $ex_id_faq);
        $this->db->set('kuantiti_saat_ini', 'kuantiti_saat_ini-' . $ex_kuantiti_detail_transaksi, FALSE);
        $query = $this->db->update('faq');
        return $query;
    }


    public function getCountFaqByKategori($kategori)
    {
        $this->db
            ->select("*");
        $this->db->from('faq');
        if ($kategori == "1") {
            $this->db->where(" faq.kuantiti_saat_ini ='0' ");
        } else if ($kategori == "2") {
            $this->db->where(" faq.kuantiti_saat_ini >'0' && faq.kuantiti_saat_ini <='5' ");
        } else if ($kategori == "3") {

            $this->db->where(" faq.kuantiti_saat_ini > '5' ");
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function delete_faq($id)
    {
        $this->db->where('id_faq', $id);
        $this->db->delete('faq');
    }

}