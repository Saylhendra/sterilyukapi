<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Status_jadwal_model extends CI_Model
{
    
    public function get_all_status_jadwal()
    {
        $this->db
            ->select("*");
        $this->db->from('status_jadwal');
        $this->db->order_by('status_jadwal.id_status_jadwal', 'ASC');
        $query = $this->db->get();

        return $query->result_array();
    }


}