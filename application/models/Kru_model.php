<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kru_model extends CI_Model
{


    public function insertkru($kru)
    {
        $query = $this->db->insert('kru', $kru);

        return $query;
    }


    function get_kru($id_jadwal, $page, $limit = null)
    {

        if ($page == null || $page == 1) {
            $page = 1;
        }

        $start = ($page - 1) * $limit;

        $this->db
            ->select("kru.*
                       , user.id_user
                       , user.nama_lengkap
                       , user.jenis_kelamin
                       , user.tempat_lahir
                       , user.tanggal_lahir
                       , user.alamat
                       , user.no_telp
                       , user.email
                       , user.foto_profil
                       , user.id_firebase
                       , user.id_level_user
                       , user.id_status_aktif
                    , tipe_kru.nama_tipe_kru");
        $this->db->from('kru');
        $this->db->join('tipe_kru', 'kru.id_tipe_kru = tipe_kru.id_tipe_kru');
        $this->db->join('user', 'kru.id_user = user.id_user');

        $this->db->where('kru.id_jadwal', $id_jadwal);

        $this->db->order_by('kru.id_kru', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        return $query->result_array();
    }

    function get_all_kru_byIdJadwal($id_jadwal)
    {
        $this->db
            ->select("kru.*
                       , user.id_user
                       , user.nama_lengkap
                       , user.jenis_kelamin
                       , user.tempat_lahir
                       , user.tanggal_lahir
                       , user.alamat
                       , user.no_telp
                       , user.email
                       , user.foto_profil
                       , user.id_firebase
                       , user.id_level_user
                       , user.id_status_aktif
                    , tipe_kru.nama_tipe_kru");
        $this->db->from('kru');
        $this->db->join('tipe_kru', 'kru.id_tipe_kru = tipe_kru.id_tipe_kru');
        $this->db->join('user', 'kru.id_user = user.id_user');

        $this->db->where('kru.id_jadwal', $id_jadwal);
        $this->db->order_by('kru.id_kru', 'DESC');
        $query = $this->db->get();

        return $query->result_array();
    }

    function deleteKruByIdJadwal($id_jadwal)
    {
        $this->db->where('id_jadwal', $id_jadwal);
        $this->db->delete('kru');
    }




}