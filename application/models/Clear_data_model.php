<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clear_data_model extends CI_Model
{

    public function cekClearDataByApplicationVersion($application_version)
    {
        $this->db
            ->select("*");
        $this->db->from('clear_data');
        $this->db->where('application_version', $application_version);
        $query = $this->db->get();

        return $query->row_array();
    }


}