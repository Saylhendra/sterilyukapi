<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Status_aktif_model extends CI_Model
{

    public function get_all_status_aktif()
    {
        $this->db
            ->select("*");
        $this->db->from('status_aktif');
        $this->db->order_by('status_aktif.id_status_aktif', 'ASC');
        $query = $this->db->get();

        return $query->result_array();
    }


}