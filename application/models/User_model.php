<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @property User_model $User_model
 */

class User_model extends CI_Model
{

    function count_user()
    {
        $this->db
            ->select("user.*");
        $this->db->from('user');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function insertuser($user)
    {
        $query = $this->db->insert('user', $user);
        return $query;
    }

    function delete_user($id)
    {
        $this->db->where('id_user', $id);
        $this->db->delete('user');
    }

    public function updateuser($id_user, $user)
    {
        $this->db->where('id_user', $id_user);
        $query = $this->db->update('user', $user);
        return $query;
    }

    //activate user account
    function verifyEmailID($key)
    {
        $data = array('id_status_aktif' => 1);
        $this->db->where('md5(email)', $key);
        return $this->db->update('user', $data);
    }

    public function UpdateIdFireBase($if, $id_user)
    {
        $query = $this->db->set('id_firebase', $if)
            ->where('id_user', $id_user)
            ->update('user');
        return $query;
    }


    public function getUserByEmail($email)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function getUserByHashEmailID($key)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('md5(email)', $key);
        $query = $this->db->get();

        return $query->row_array();
    }


    function get_user($page, $search)
    {
        if ($page == null) {
            $page = "1";
        }
        $limit = "10";
        $this->db
            ->select("user.*");
        $this->db->from('user');
        if ($search != null) {
            $this->db->like('nama_lengkap', $search);
            $this->db->or_like('nama_lengkap', $search, 'after');
            $this->db->or_like('nama_lengkap', $search, 'before');
            $this->db->or_like('email', $search);
            $this->db->or_like('email', $search, 'after');
            $this->db->or_like('email', $search, 'before');
        }
        $this->db->order_by('user.id_user', 'DESC');
        $this->db->group_by('id_user');
        $this->db->limit($limit, $page - 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_role($page, $search)
    {
        if ($page == null) {
            $page = "1";
        }
        $limit = "10";
        $this->db
            ->select("user.*");
        $this->db->from('user');
        $this->db->where('id_level_user !=','3');
        if ($search != null) {
            $this->db->like('nama_lengkap', $search);
            $this->db->or_like('nama_lengkap', $search, 'after');
            $this->db->or_like('nama_lengkap', $search, 'before');
            $this->db->or_like('email', $search);
            $this->db->or_like('email', $search, 'after');
            $this->db->or_like('email', $search, 'before');
        }
        $this->db->order_by('user.id_user', 'DESC');
        $this->db->group_by('id_user');
        $this->db->limit($limit, $page - 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_tim()
    {
        $this->db
            ->select("user.*");
        $this->db->from('user');
        $this->db->where("(id_level_user='2' OR id_level_user='4')", NULL, FALSE);
        $this->db->order_by('user.nama_lengkap', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getUserById($id_user)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('id_user', $id_user);
        $query = $this->db->get();

        return $query->row_array();
    }


    public function login($e, $p)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('email', $e);
        $this->db->where('password', $p);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function cekemail($e)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('email', $e);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function cekpassword($i, $p)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('id_user', $i);
        $this->db->where('password', $p);
        $query = $this->db->get();

        return $query->row_array();
    }


    public function register($data)
    {
        $query = $this->db->insert('user', $data);;

        return $query;
    }

    public function resetpassword($e, $data)
    {
        $this->db->where('email', $e);
        $query = $this->db->update('user', $data);

        return $query;
    }


}