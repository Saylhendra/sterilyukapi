<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Status_book_model extends CI_Model
{
    
    public function get_all_status_book()
    {
        $this->db
            ->select("*");
        $this->db->from('status_book');
        $this->db->order_by('status_book.id_status_book', 'ASC');
        $query = $this->db->get();

        return $query->result_array();
    }


}