<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran_pendaftaran_peserta_model extends CI_Model
{


    public function insertpembayaran_pendaftaran_peserta($pembayaran_pendaftaran_peserta)
    {
        $query = $this->db->insert('pembayaran_pendaftaran_peserta', $pembayaran_pendaftaran_peserta);
        return $query;
    }

    public function updatepembayaran_pendaftaran_peserta($id_pembayaran_pendaftaran_peserta, $pembayaran_pendaftaran_peserta)
    {
        $this->db->where('id_pembayaran_pendaftaran_peserta', $id_pembayaran_pendaftaran_peserta);
        $query = $this->db->update('pembayaran_pendaftaran_peserta', $pembayaran_pendaftaran_peserta);
        return $query;
    }

    public function updatepembayaran_pendaftaran_pesertaByIdBook($id_book, $pembayaran_pendaftaran_peserta)
    {
        $this->db->where('id_book', $id_book);
        $query = $this->db->update('pembayaran_pendaftaran_peserta', $pembayaran_pendaftaran_peserta);
        return $query;
    }



}