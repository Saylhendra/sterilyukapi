<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('User_model', '', TRUE);
        $this->load->model('Jadwal_model', '', TRUE);
        $this->load->model('Kru_model', '', TRUE);
        $this->load->model('Book_model', '', TRUE);
        $this->load->model('Lokasi_jadwal_model', '', TRUE);
        $this->load->model('Faq_model', '', TRUE);

    }

    public function index()
    {
        echo "Access Denied";
    }

    function home()
    {
        $id_user = $this->input->post('id_user');
        $waktu_mulai_jadwal = $this->input->post('waktu_mulai_jadwal');
        $response['count_jadwal_barang_masuk'] = $this->Jadwal_model->count_jadwal($id_user, $waktu_mulai_jadwal, "BM");
        $response['count_jadwal_barang_keluar'] = $this->Jadwal_model->count_jadwal($id_user, $waktu_mulai_jadwal, "BK");
        $response['count_user'] = $this->User_model->count_user();
        $response['habis'] = $this->Lokasi_jadwal_model->getCountBarangByKategori(1);
        $response['menipis'] = $this->Lokasi_jadwal_model->getCountBarangByKategori(2);
        $response['tersedia'] = $this->Lokasi_jadwal_model->getCountBarangByKategori(3);
        echo json_encode($response);
    }

    function export_excel_jadwal($waktu_mulai_jadwal = null, $kode_tipe_jadwal = null)
    {
        if ($kode_tipe_jadwal == "BM") {
            $k = "Barang Masuk";
            $jdl = "Harga Satuan Pembelian";
        } else {
            $k = "Barang Keluar";
            $jdl = "Harga Satuan Penjualan";
        }
        $wkt = $this->formatIndonesia($waktu_mulai_jadwal);

        if ($waktu_mulai_jadwal != null && $k != null) {
            $title = "Data Jadwal " . $k . " - " . $wkt;
            $record = $this->Book_model->export_book($waktu_mulai_jadwal, $kode_tipe_jadwal);

            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle($k . " - " . $wkt);

            $this->excel->getActiveSheet()->setCellValue('A1', $title);
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->mergeCells('A1:AA1');
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


            $this->excel->getActiveSheet()->setCellValue('A3', "Waktu Jadwal");
            $this->excel->getActiveSheet()->mergeCells('A3:C4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('A3:C4')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('D3', "Kode Jadwal");
            $this->excel->getActiveSheet()->mergeCells('D3:F4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('D3:F4')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('G3', "Petugas");
            $this->excel->getActiveSheet()->mergeCells('G3:I4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('G3:I4')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('J3', "Detail Jadwal");
            $this->excel->getActiveSheet()->mergeCells('J3:AA3');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('J3:AA3')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('J4', "Kode Barang");
            $this->excel->getActiveSheet()->mergeCells('J4:L4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('J4:L4')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('M4', "Nama Barang");
            $this->excel->getActiveSheet()->mergeCells('M4:O4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('M4:O4')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('P4', "Kuantiti");
            $this->excel->getActiveSheet()->mergeCells('P4:R4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('P4:R4')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('S4', "Keterangan");
            $this->excel->getActiveSheet()->mergeCells('S4:U4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('S4:U4')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('V4', $jdl);
            $this->excel->getActiveSheet()->mergeCells('V4:X4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('V4:X4')->applyFromArray($BStyle);

            $this->excel->getActiveSheet()->setCellValue('Y4', "Total Per Items");
            $this->excel->getActiveSheet()->mergeCells('Y4:AA4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('Y4:AA4')->applyFromArray($BStyle);

            /*$this->excel->getActiveSheet()->setCellValue('Y3', "Total Per Items");
            $this->excel->getActiveSheet()->mergeCells('Y3:AA4');
            $BStyle = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );
            $this->excel->getActiveSheet()->getStyle('Y3:AA4')->applyFromArray($BStyle);*/

            $this->excel->getActiveSheet()->getStyle('A3:AA4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A3:AA4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $this->excel->getActiveSheet()
                ->getStyle('A3:AA4')
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('5B799E');

            $styleArray = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ));

            $this->excel->getActiveSheet()->getStyle('A3:AA4')->applyFromArray($styleArray);

            if (is_array($record)) {
                $wt = "";
                $kt = "";
                $iu = "";
                $no = 5;
                $r = 0;
                $total_kesuluruhan_jadwal = 0;
                foreach ($record as $row) :
                    $j = $no + $row["jmlh_item"] - 1;
                    $w = explode(" ", $row['waktu_mulai_jadwal']);
                    if ($row['waktu_mulai_jadwal'] != $wt) {
                        $this->excel->getActiveSheet()->setCellValue("A$no", $w[1]);
                        $this->excel->getActiveSheet()->mergeCells("A$no:C$j");
                        $this->excel->getActiveSheet()->getStyle("A$no:C$j")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                        $BStyle = array(
                            'borders' => array(
                                'outline' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array('rgb' => 'AAAAAA')
                                )
                            )
                        );
                        $this->excel->getActiveSheet()->getStyle("A$no:C$j")->applyFromArray($BStyle);
                    }
                    if ($row['kode_jadwal'] != $kt) {
                        $this->excel->getActiveSheet()->setCellValue("D$no", $row['kode_jadwal']);
                        $this->excel->getActiveSheet()->mergeCells("D$no:F$j");
                        $this->excel->getActiveSheet()->getStyle("D$no:F$j")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                        $BStyle = array(
                            'borders' => array(
                                'outline' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array('rgb' => 'AAAAAA')
                                )
                            )
                        );
                        $this->excel->getActiveSheet()->getStyle("D$no:F$j")->applyFromArray($BStyle);
                    }
                    if ($row['waktu_mulai_jadwal'] != $wt) {
                        if ($r % 2 == 0) {
                            $this->excel->getActiveSheet()
                                ->getStyle("A$no:AA$j")
                                ->getFill()
                                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('DFDFDF');
                        } else {
                            $this->excel->getActiveSheet()
                                ->getStyle("A$no:AA$j")
                                ->getFill()
                                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                                ->getStartColor()
                                ->setRGB('EEEEEE');
                        }

                        $this->excel->getActiveSheet()->setCellValue("G$no", $row['nama']);
                        $this->excel->getActiveSheet()->mergeCells("G$no:I$j");
                        $this->excel->getActiveSheet()->getStyle("G$no:I$j")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                        $BStyle = array(
                            'borders' => array(
                                'outline' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array('rgb' => 'AAAAAA')
                                )
                            )
                        );
                        $this->excel->getActiveSheet()->getStyle("G$no:I$j")->applyFromArray($BStyle);

                        $r++;
                    }

                    $this->excel->getActiveSheet()->setCellValue("J$no", $row["kode_barang"]);
                    $this->excel->getActiveSheet()->mergeCells("J$no:L$no");
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => 'AAAAAA')
                            )
                        )
                    );
                    $this->excel->getActiveSheet()->getStyle("J$no")->applyFromArray($BStyle);

                    $this->excel->getActiveSheet()->setCellValue("M$no", $row["nama_barang"]);
                    $this->excel->getActiveSheet()->mergeCells("M$no:O$no");
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => 'AAAAAA')
                            )
                        )
                    );
                    $this->excel->getActiveSheet()->getStyle("M$no")->applyFromArray($BStyle);

                    $this->excel->getActiveSheet()->setCellValue("P$no", $row["kuantiti_book"]);
                    $this->excel->getActiveSheet()->mergeCells("P$no:R$no");
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => 'AAAAAA')
                            )
                        )
                    );
                    $this->excel->getActiveSheet()->getStyle("P$no")->applyFromArray($BStyle);

                    $this->excel->getActiveSheet()->setCellValue("S$no", $row["keterangan_book"]);
                    $this->excel->getActiveSheet()->mergeCells("S$no:U$no");
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => 'AAAAAA')
                            )
                        )
                    );
                    $this->excel->getActiveSheet()->getStyle("S$no")->applyFromArray($BStyle);

                    $this->excel->getActiveSheet()->setCellValue("V$no", $this->buatrp($row["harga_satuan"]));
                    $this->excel->getActiveSheet()->mergeCells("V$no:X$no");
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => 'AAAAAA')
                            )
                        )
                    );
                    $this->excel->getActiveSheet()->getStyle("V$no")->applyFromArray($BStyle);

                    $_kuantiti_book = (int)$row["kuantiti_book"];
                    $_harga_satuan = (int)$row["harga_satuan"];
                    $total_per_item = $_kuantiti_book * $_harga_satuan;

                    $total_kesuluruhan_jadwal = $total_kesuluruhan_jadwal + $total_per_item;

                    $this->excel->getActiveSheet()->setCellValue("Y$no", $this->buatrp($total_per_item));
                    $this->excel->getActiveSheet()->mergeCells("Y$no:AA$no");
                    $BStyle = array(
                        'borders' => array(
                            'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => 'AAAAAA')
                            )
                        )
                    );
                    $this->excel->getActiveSheet()->getStyle("Y$no")->applyFromArray($BStyle);


                    $wt = $row["waktu_mulai_jadwal"];
                    $kt = $row["kode_jadwal"];
                    $iu = $row["id_user"];
                    $no++;
                endforeach;

                $BStyle = array(
                    'borders' => array(
                        'outline' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '666666')
                        )
                    )
                );
                $n = $no - 1;
                $this->excel->getActiveSheet()->getStyle("A3:AA$n")->applyFromArray($BStyle);

                $nn = $n + 2;
                $nnn = $n + 3;
                $this->excel->getActiveSheet()->setCellValue("A$nn", "Total Harga Keseluruhan Jadwal " . $k . " - " . $wkt);
                $this->excel->getActiveSheet()->setCellValue("A$nnn", $this->buatrp($total_kesuluruhan_jadwal));

                $this->excel->getActiveSheet()->getStyle("A$nn")->getFont()->setSize(12);
                $this->excel->getActiveSheet()->getStyle("A$nn")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->mergeCells("A$nn:M$nn");

                $this->excel->getActiveSheet()->getStyle("A$nnn")->getFont()->setSize(16);
                $this->excel->getActiveSheet()->getStyle("A$nnn")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->mergeCells("A$nnn:F$nnn");


            } else {
            }

            $filename = str_replace(" ", "_", $title) . ".xls"; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
//force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');

            //   $this->load->view('export_excel_jadwal', $data);
        } else {
            echo "Harap masukan waktu jadwal dan kode jadwal";
        }
    }

    function buatrp($angka)
    {
        $jadi = "Rp " . number_format($angka, 0, ',', '.');
        return $jadi;
    }

    function formatIndonesia($waktu_mulai_jadwal)
    {
        $w = explode(" ", $waktu_mulai_jadwal);
        $x = explode("-", $w[0]);
        $bulan = "";
        if ($x[1] == "01") {
            $bulan = "Januari";
        }
        if ($x[1] == "02") {
            $bulan = "Februari";
        }
        if ($x[1] == "03") {
            $bulan = "Maret";
        }
        if ($x[1] == "04") {
            $bulan = "April";
        }
        if ($x[1] == "05") {
            $bulan = "Mei";
        }
        if ($x[1] == "06") {
            $bulan = "Juni";
        }
        if ($x[1] == "07") {
            $bulan = "Juli";
        }
        if ($x[1] == "08") {
            $bulan = "Agustus";
        }
        if ($x[1] == "09") {
            $bulan = "September";
        }
        if ($x[1] == "10") {
            $bulan = "Oktober";
        }
        if ($x[1] == "11") {
            $bulan = "November";
        }
        if ($x[1] == "12") {
            $bulan = "Desember";
        }

        return $x[2] . " " . $bulan . " " . $x[0];

    }

    function jadwal($kode_tipe_jadwal, $page = null, $id_user = null, $waktu_mulai_jadwal = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['jadwal'] = $this->Jadwal_model->get_jadwal($kode_tipe_jadwal, $page, $id_user, $waktu_mulai_jadwal);

        echo json_encode($response);
    }

    function detail_jadwal($id_jadwal, $id_user = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_jadwal = $this->Jadwal_model->getJadwalById($id_jadwal, $id_user);

        if ($detail_jadwal == null) {
            $response['isSuccess'] = false;
            $response['message'] = "not available";
        }

        $kru_jadwal = $this->Kru_model->get_kru($id_jadwal, 1, 3);
        $book_jadwal = $this->Book_model->get_book($id_jadwal, 1, 3);
        $response['id_jadwal'] = $detail_jadwal["id_jadwal"];
        $response['kode_jadwal'] = $detail_jadwal["kode_jadwal"];
        $response['waktu_mulai_jadwal'] = $detail_jadwal["waktu_mulai_jadwal"];
        $response['waktu_akhir_jadwal'] = $detail_jadwal["waktu_akhir_jadwal"];
        $response['keterangan_jadwal'] = $detail_jadwal["keterangan_jadwal"];
        $response['kuota_jantan'] = $detail_jadwal["kuota_jantan"];
        $response['kuota_betina'] = $detail_jadwal["kuota_betina"];
        $response['sisa_kuota_jantan'] = $detail_jadwal["sisa_kuota_jantan"];
        $response['sisa_kuota_betina'] = $detail_jadwal["sisa_kuota_betina"];
        $response['jumlah_book'] = $detail_jadwal["jumlah_book"];
        $response['jumlah_komentar'] = $detail_jadwal["jumlah_komentar"];
        $response['waktu_dibuat'] = $detail_jadwal["waktu_dibuat"];
        $response['id_tipe_jadwal'] = $detail_jadwal["id_tipe_jadwal"];
        $response['id_lokasi_jadwal'] = $detail_jadwal["id_lokasi_jadwal"];
        $response['id_status_aktif'] = $detail_jadwal["id_status_aktif"];
        $response['kode_tipe_jadwal'] = $detail_jadwal["kode_tipe_jadwal"];
        $response['nama_tipe_jadwal'] = $detail_jadwal["nama_tipe_jadwal"];
        $response['nama_lokasi_jadwal'] = $detail_jadwal["nama_lokasi_jadwal"];
        $response['alamat_lokasi_jadwal'] = $detail_jadwal["alamat_lokasi_jadwal"];
        $response['no_telp_lokasi_jadwal'] = $detail_jadwal["no_telp_lokasi_jadwal"];
        $response['latitude_lokasi_jadwal'] = $detail_jadwal["latitude_lokasi_jadwal"];
        $response['longitude_lokasi_jadwal'] = $detail_jadwal["longitude_lokasi_jadwal"];
        $response['id_user'] = $detail_jadwal["id_user"];
        $response['id_status_book'] = $detail_jadwal["id_status_book"];
        $response['nama_status_book'] = $detail_jadwal["nama_status_book"];
        $response['kru'] = $kru_jadwal;
        $response['book'] = $book_jadwal;
        echo json_encode($response);
    }

    function book()
    {
        $limit = $this->input->post('limit');
        $status = $this->input->post('status');
        $kode_jadwal = $this->input->post('kode_jadwal');
        $id_book_terbaru = $this->input->post('id_book_terbaru');
        $id_book_terlama = $this->input->post('id_book_terlama');

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($kode_jadwal != null) {
            $response['isSuccess'] = true;
            $response['message'] = "berhasil";
            $response['book'] = $this->Book_model->get_book($status, $kode_jadwal, $id_book_terbaru, $id_book_terlama, $limit);
        }
        echo json_encode($response);
    }


    function count_barang()
    {
        $response['habis'] = $this->Lokasi_jadwal_model->getCountBarangByKategori(1);
        $response['menipis'] = $this->Lokasi_jadwal_model->getCountBarangByKategori(2);
        $response['tersedia'] = $this->Lokasi_jadwal_model->getCountBarangByKategori(3);
        echo json_encode($response);
    }

    function detail_lokasi_jadwal($id_lokasi_jadwal)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_lokasi_jadwal = $this->Lokasi_jadwal_model->getLokasiJadwalById($id_lokasi_jadwal);
        $response['id_lokasi_jadwal'] = $detail_lokasi_jadwal["id_lokasi_jadwal"];
        $response['nama_lokasi_jadwal'] = $detail_lokasi_jadwal["nama_lokasi_jadwal"];
        $response['alamat_lokasi_jadwal'] = $detail_lokasi_jadwal["alamat_lokasi_jadwal"];
        $response['no_telp_lokasi_jadwal'] = $detail_lokasi_jadwal["no_telp_lokasi_jadwal"];
        $response['latitude_lokasi_jadwal'] = $detail_lokasi_jadwal["latitude_lokasi_jadwal"];
        $response['longitude_lokasi_jadwal'] = $detail_lokasi_jadwal["longitude_lokasi_jadwal"];
        echo json_encode($response);
    }

    function lokasi_jadwal($page = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['lokasi_jadwal'] = $this->Lokasi_jadwal_model->get_lokasi_jadwal($page);
        echo json_encode($response);
    }

    function all_lokasi_jadwal()
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['lokasi_jadwal'] = $this->Lokasi_jadwal_model->get_all_lokasi_jadwal();
        echo json_encode($response);
    }


    function all_faq()
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['faq'] = $this->Faq_model->get_all_faq();
        echo json_encode($response);
    }

    function faq($page = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['faq'] = $this->Faq_model->get_faq($page);
        echo json_encode($response);
    }


    function detail_faq($id_faq)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_faq = $this->Faq_model->getFaqById($id_faq);
        if ($detail_faq == null) {
            $response['isSuccess'] = false;
            $response['message'] = "not available";
        }
        $response['id_faq'] = $detail_faq["id_faq"];
        $response['question_faq'] = $detail_faq["question_faq"];
        $response['answer_faq'] = $detail_faq["answer_faq"];
        echo json_encode($response);
    }

    function addeditfaq()
    {
        $id_faq = $this->input->post('id_faq');
        $question_faq = $this->input->post('question_faq');
        $answer_faq = $this->input->post('answer_faq');
        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($question_faq != null || $answer_faq != null) {
            $faq = array(
                'question_faq' => $question_faq,
                'answer_faq' => $answer_faq,

            );

            $nb = $this->Faq_model->cekquestionfaq($question_faq);

            if ($id_faq != null) {
                if (strtolower($nb["question_faq"]) == strtolower($question_faq) || $nb == null) {
                    $action_faq = $this->Faq_model->updatefaq($id_faq, $faq);
                    if ($action_faq) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil mengedit faq";
                    } else {
                        $response['message'] = "gagal mengedit faq";
                    }
                } else {
                    $response['message'] = "Pertanyaan sudah ada...";
                }

            } else {
                if ($nb != null) {
                    $response['message'] = "Pertanyaan sudah ada...";
                } else {
                    $action_faq = $this->Faq_model->insertfaq($faq);
                    if ($action_faq) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil menambah faq";
                    } else {
                        $response['message'] = "gagal menambah faq";
                    }
                }
            }
        }
        echo json_encode($response);
    }

    function delete_faq($id)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil menghapus faq";
        $this->Faq_model->delete_faq($id);
        echo json_encode($response);
    }


    function addjadwal()
    {

        $id_tipe_jadwal = $this->input->post('id_tipe_jadwal');
        $kode_jadwal = $this->input->post('kode_jadwal');
        $waktu_mulai_jadwal = $this->input->post('waktu_mulai_jadwal');
        $keterangan_jadwal = $this->input->post('keterangan_jadwal');
        $total_harga_jadwal = $this->input->post('total_harga_jadwal');
        $diskon_jadwal = $this->input->post('diskon_jadwal');
        $harga_akhir_jadwal = $this->input->post('harga_akhir_jadwal');
        $pembayaran_tunai_jadwal = $this->input->post('pembayaran_tunai_jadwal');
        $kembalian_jadwal = $this->input->post('kembalian_jadwal');
        $id_user = $this->input->post('id_user');

        $id_barang = $this->input->post('id_barang');
        $kuantiti_book = $this->input->post('kuantiti_book');
        $harga_satuan = $this->input->post('harga_satuan');
        $keterangan_book = $this->input->post('keterangan_book');

        if ($keterangan_jadwal == null) {
            $keterangan_jadwal = " - ";
        }

        $ex_id_barang = explode(" | ", $id_barang);
        $ex_kuantiti_book = explode(" | ", $kuantiti_book);
        $ex_harga_satuan = explode(" | ", $harga_satuan);
        $ex_keterangan_book = explode(" | ", $keterangan_book);

        $jmlh_data = count($ex_id_barang);

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($kode_jadwal != null || $id_tipe_jadwal != null || $waktu_mulai_jadwal != null || $id_user != null || $total_harga_jadwal != null) {
            $jadwal = array(
                'id_tipe_jadwal' => $id_tipe_jadwal,
                'kode_jadwal' => $kode_jadwal,
                'waktu_mulai_jadwal' => $waktu_mulai_jadwal,
                'keterangan_jadwal' => $keterangan_jadwal,
                'total_harga_jadwal' => $total_harga_jadwal,
                'diskon_jadwal' => $diskon_jadwal,
                'harga_akhir_jadwal' => $harga_akhir_jadwal,
                'pembayaran_tunai_jadwal' => $pembayaran_tunai_jadwal,
                'kembalian_jadwal' => $kembalian_jadwal,
                'id_user' => $id_user

            );

            $action_jadwal = $this->Jadwal_model->insertjadwal($jadwal);

            if ($action_jadwal) {

                $tr = $this->Jadwal_model->getJadwalByKodeJadwal($kode_jadwal);

                for ($x = 0; $x < $jmlh_data; $x++) {
                    $book = array(
                        'id_jadwal' => $tr["id_jadwal"],
                        'id_barang' => $ex_id_barang[$x],
                        'kuantiti_book' => $ex_kuantiti_book[$x],
                        'harga_satuan' => $ex_harga_satuan[$x],
                        'keterangan_book' => $ex_keterangan_book[$x]

                    );

                    if ($ex_id_barang[$x] != "") {
                        $dt = $this->Book_model->insertbook($book);
                        if ($dt) {
                            if ($id_tipe_jadwal == "1") {
                                $this->Lokasi_jadwal_model->tambah_kuantiti_barang($ex_id_barang[$x], $ex_kuantiti_book[$x]);
                            } else if ($id_tipe_jadwal == "2")
                                $this->Lokasi_jadwal_model->kurang_kuantiti_barang($ex_id_barang[$x], $ex_kuantiti_book[$x]);
                        }
                    }
                }
                $response['isSuccess'] = true;
                $response['message'] = "berhasil menambah jadwal";
            } else {
                $response['message'] = "gagal menambah jadwal";
            }
        }
        echo json_encode($response);
    }

    function addeditlokasi_jadwal()
    {
        $id_lokasi_jadwal = $this->input->post('id_lokasi_jadwal');
        $nama_lokasi_jadwal = $this->input->post('nama_lokasi_jadwal');
        $alamat_lokasi_jadwal = $this->input->post('alamat_lokasi_jadwal');
        $no_telp_lokasi_jadwal = $this->input->post('no_telp_lokasi_jadwal');
        $latitude_lokasi_jadwal = $this->input->post('latitude_lokasi_jadwal');
        $longitude_lokasi_jadwal = $this->input->post('longitude_lokasi_jadwal');

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($nama_lokasi_jadwal != null || $alamat_lokasi_jadwal != null || $no_telp_lokasi_jadwal != null || $latitude_lokasi_jadwal != null || $longitude_lokasi_jadwal != null) {
            $lokasi_jadwal = array(
                'nama_lokasi_jadwal' => $nama_lokasi_jadwal,
                'alamat_lokasi_jadwal' => $alamat_lokasi_jadwal,
                'no_telp_lokasi_jadwal' => $no_telp_lokasi_jadwal,
                'latitude_lokasi_jadwal' => $latitude_lokasi_jadwal,
                'longitude_lokasi_jadwal' => $longitude_lokasi_jadwal

            );

            $nb = $this->Lokasi_jadwal_model->ceknamalokasi_jadwal($nama_lokasi_jadwal);

            if ($id_lokasi_jadwal != null) {
                if (strtolower($nb["nama_lokasi_jadwal"]) == strtolower($nama_lokasi_jadwal) || $nb == null) {
                    $action_lokasi_jadwal = $this->Lokasi_jadwal_model->updatelokasi_jadwal($id_lokasi_jadwal, $lokasi_jadwal);
                    if ($action_lokasi_jadwal) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil mengedit lokasi_jadwal";
                    } else {
                        $response['message'] = "gagal mengedit lokasi_jadwal";
                    }
                } else {
                    $response['message'] = "Nama Lokasi sudah ada...";
                }

            } else {
                if ($nb != null) {
                    $response['message'] = "Nama Lokasi sudah ada...";
                } else {
                    $action_lokasi_jadwal = $this->Lokasi_jadwal_model->insertlokasi_jadwal($lokasi_jadwal);
                    if ($action_lokasi_jadwal) {
                        $response['isSuccess'] = true;
                        $response['message'] = "berhasil menambah lokasi_jadwal";
                    } else {
                        $response['message'] = "gagal menambah lokasi_jadwal";
                    }
                }
            }
        }
        echo json_encode($response);
    }

    function delete_lokasi_jadwal($id)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil menghapus lokasi_jadwal";
        $this->Lokasi_jadwal_model->delete_lokasi_jadwal($id);
        echo json_encode($response);
    }


    function user($page = null)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $response['user'] = $this->User_model->get_user($page);
        echo json_encode($response);
    }


    function detail_user($id_user)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil";
        $detail_user = $this->User_model->getUserById($id_user);
        if ($detail_user == null) {
            $response['isSuccess'] = false;
            $response['message'] = "not available";
        }
        $response['id_user'] = $detail_user["id_user"];
        $response['nama_lengkap'] = $detail_user["nama_lengkap"];
        $response['username'] = $detail_user["username"];
        $response['password'] = $detail_user["password"];
        $response['foto_profil'] = $detail_user["password"];
        $response['id_level_user'] = $detail_user["password"];
        $response['id_status_aktif'] = $detail_user["password"];

        echo json_encode($response);
    }

    function delete_user($id)
    {
        $response['isSuccess'] = true;
        $response['message'] = "berhasil menghapus user";
        $this->User_model->delete_user($id);
        echo json_encode($response);
    }


    function addedituser()
    {
        $id_user = $this->input->post('id_user');
        $username = $this->input->post('username');
        $nama = $this->input->post('nama');
        $password = md5($this->input->post('password'));
        $real_password = $this->input->post('password');
        $id_level_user = $this->input->post('id_level_user');
        $id_status_aktif = $this->input->post('id_status_aktif');


        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($username != null || $nama != null || $password != null || $id_level_user != null || $id_status_aktif != null) {
            $user = array(
                'username' => $username,
                'nama' => $nama,
                'password' => $password,
                'real_password' => $real_password,
                'id_level_user' => $id_level_user,
                'id_status_aktif' => $id_status_aktif

            );

            $kb = $this->User_model->cekusername($username);
            if ($id_user != null) {
                if (strtolower($kb["username"]) == strtolower($username) || $kb == null) {
                    $action_user = $this->User_model->updateuser($id_user, $user);
                    if ($action_user) {
                        $result_user = $this->User_model->getUserByUsername($username);
                        if ($result_user != null) {
                            $response['isSuccess'] = true;
                            $response['user'] = $result_user;
                            $response['message'] = "berhasil mengedit user";
                        } else {
                            $response['message'] = "gagal mengedit user";
                        }
                    } else {
                        $response['message'] = "gagal mengedit user";
                    }

                } else {
                    $response['message'] = "Username sudah ada...";
                }


            } else {
                if ($kb != null) {
                    $response['message'] = "Username sudah ada...";
                } else {
                    $action_user = $this->User_model->insertuser($user);
                    if ($action_user) {
                        $result_user = $this->User_model->getUserByUsername($username);
                        if ($result_user != null) {
                            $response['isSuccess'] = true;
                            $response['user'] = $result_user;
                            $response['message'] = "berhasil menambah user";
                        } else {
                            $response['message'] = "gagal menambah user";
                        }
                    } else {
                        $response['message'] = "gagal menambah user";
                    }
                }
            }
        }
        echo json_encode($response);
    }

    function login()
    {
        $u = $this->input->post('username');
        $p = md5($this->input->post('password'));

        $response['isSuccess'] = false;
        $response['message'] = "Error";
        if ($u != null && $p != null) {
            $user = $this->User_model->login($u, $p);
            if ($user != null) {
                if ($user["id_status_aktif"] == "1") {
                    $response['isSuccess'] = true;
                    $response['message'] = "Data user ditemukan";
                    $response['user'] = $user;
                } else {
                    $response['message'] = "Username Tidak Aktif . Harap Hubungi Admin";
                }
            } else {
                $response['message'] = "Username atau password anda salah";
            }
        }
        echo json_encode($response);
    }


}
