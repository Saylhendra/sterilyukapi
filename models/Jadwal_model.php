<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal_model extends CI_Model
{


    public function insertjadwal($jadwal)
    {
        $query = $this->db->insert('jadwal', $jadwal);

        return $query;
    }

    public function getJadwalByKodeJadwal($kode_jadwal)
    {
        $this->db
            ->select("*");
        $this->db->from('jadwal');
        $this->db->where('kode_jadwal', $kode_jadwal);
        $query = $this->db->get();

        return $query->row_array();
    }

    function get_jadwal($kode_tipe_jadwal, $page, $id_user=null, $waktu_mulai_jadwal = null)
    {
        if ($id_user == null) {
            $id_user="0";
        }
        if ($page == null) {
            $page=1;
        }
        $limit = "10";

        $this->db
            ->select("jadwal.id_jadwal as idnya,
            jadwal.*,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            lokasi_jadwal.*,
            (SELECT id_user FROM book WHERE id_jadwal=idnya AND id_user='".$id_user."') AS id_user,
            (SELECT id_status_book FROM book WHERE id_jadwal=idnya AND id_user='".$id_user."') AS id_status_book,
            (SELECT nama_status_book FROM book INNER JOIN status_book ON (book.id_status_book = status_book.id_status_book) WHERE id_jadwal=idnya AND id_user='".$id_user."') AS nama_status_book");

        $this->db->from('jadwal');

        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('lokasi_jadwal', 'lokasi_jadwal.id_lokasi_jadwal = jadwal.id_lokasi_jadwal');

        $this->db->where('tipe_jadwal.kode_tipe_jadwal', $kode_tipe_jadwal);

        if ($waktu_mulai_jadwal != null) {
            $this->db->like('jadwal.waktu_mulai_jadwal', $waktu_mulai_jadwal);
        }

        $this->db->order_by('jadwal.id_jadwal', 'DESC');
        $this->db->limit($limit, $page-1);

        $query = $this->db->get();


        return $query->result_array();
    }

    public function getJadwalById($id_jadwal,$id_user=null)
    {
        if ($id_user == null) {
            $id_user="0";
        }
        $this->db
            ->select("jadwal.id_jadwal as idnya,
            jadwal.*,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            lokasi_jadwal.*,
            (SELECT id_user FROM book WHERE id_jadwal=idnya AND id_user='".$id_user."') AS id_user,
            (SELECT id_status_book FROM book WHERE id_jadwal=idnya AND id_user='".$id_user."') AS id_status_book,
            (SELECT nama_status_book FROM book INNER JOIN status_book ON (book.id_status_book = status_book.id_status_book) WHERE id_jadwal=idnya AND id_user='".$id_user."') AS nama_status_book");

        $this->db->from('jadwal');

        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('lokasi_jadwal', 'lokasi_jadwal.id_lokasi_jadwal = jadwal.id_lokasi_jadwal');

        $this->db->where('jadwal.id_jadwal', $id_jadwal);

        $query = $this->db->get();

        return $query->row_array();
    }

    function count_jadwal($id_user = null, $waktu_mulai_jadwal = null, $kode_tipe_jadwal = null)
    {

        $this->db
            ->select("jadwal.id_jadwal as idnya,
            jadwal.*,
            tipe_jadwal.kode_tipe_jadwal,
            tipe_jadwal.nama_tipe_jadwal,
            user.nama,
            (SELECT COUNT(id_detail_jadwal) AS count_item FROM detail_jadwal where id_jadwal=idnya)AS count_item");

        $this->db->from('jadwal');

        $this->db->join('tipe_jadwal', 'tipe_jadwal.id_tipe_jadwal = jadwal.id_tipe_jadwal');
        $this->db->join('user', 'user.id_user = jadwal.id_user');

        if ($id_user != null) {
            $this->db->where('jadwal.id_user', $id_user);
        }
        if ($kode_tipe_jadwal != null) {
            $this->db->where('tipe_jadwal.kode_tipe_jadwal', $kode_tipe_jadwal);

        }
        if ($waktu_mulai_jadwal != null) {
            $this->db->like('jadwal.waktu_mulai_jadwal', $waktu_mulai_jadwal);
        }

        $query = $this->db->get();

        return $query->num_rows();
    }
}