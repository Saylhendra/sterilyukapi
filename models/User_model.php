<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{

    function count_user()
    {
        $this->db
            ->select("user.*");
        $this->db->from('user');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function insertuser($user)
    {
        $query = $this->db->insert('user', $user);
        return $query;
    }
    function delete_user($id)
    {
        $this->db->where('id_user', $id);
        $this->db->delete('user');
    }

    public function updateuser($id_user, $user)
    {
        $this->db->where('id_user', $id_user);
        $query = $this->db->update('user', $user);
        return $query;
    }
    public function getUserByUsername($username)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('username', $username);
        $query = $this->db->get();

        return $query->row_array();
    }


    function get_user($page)
    {
        if ($page == null) {
            $page="1";
        }
        $limit = "10";
        $this->db
            ->select("user.*");
        $this->db->from('user');
        $this->db->order_by('user.id_user', 'DESC');
        $this->db->limit($limit, $page-1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUserById($id_user)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('id_user', $id_user);
        $query = $this->db->get();

        return $query->row_array();
    }


    public function login($u, $p)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('username', $u);
        $this->db->where('password', $p);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function cekusername($u)
    {
        $this->db
            ->select("*");
        $this->db->from('user');
        $this->db->where('username', $u);
        $query = $this->db->get();

        return $query->row_array();
    }


    public function register($data)
    {
        $query = $this->db->insert('user', $data);;

        return $query;
    }

    public function resetpassword($e, $data)
    {
        $this->db->where('email', $e);
        $query = $this->db->update('user', $data);

        return $query;
    }


}