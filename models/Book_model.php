<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Book_model extends CI_Model
{


    public function insertbook($book)
    {
        $query = $this->db->insert('book', $book);

        return $query;
    }


    function get_book($id_jadwal, $page, $limit = null)
    {

        if ($limit == null) {
            $limit = "10";
        }

        $this->db
            ->select("book.*
                    , user.nama_lengkap
                    , user.foto_profil
                    , status_book.nama_status_book");
        $this->db->from('book');

        $this->db->join('status_book', 'book.id_status_book = status_book.id_status_book');
        $this->db->join('user', 'book.id_user = user.id_user');

        $this->db->where('book.id_jadwal', $id_jadwal);

        $this->db->order_by('book.id_book', 'DESC');
        $this->db->limit($limit, $page-1);
        $query = $this->db->get();

        return $query->result_array();
    }


}