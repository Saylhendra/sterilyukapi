<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kru_model extends CI_Model
{


    public function insertkru($kru)
    {
        $query = $this->db->insert('kru', $kru);

        return $query;
    }


    function get_kru($id_jadwal, $page, $limit = null)
    {

        if ($limit == null) {
            $limit = "10";
        }

        $this->db
            ->select("kru.*
                    , user.nama_lengkap
                    , user.foto_profil
                    , tipe_kru.nama_tipe_kru");
        $this->db->from('kru');
        $this->db->join('tipe_kru', 'kru.id_tipe_kru = tipe_kru.id_tipe_kru');
        $this->db->join('user', 'kru.id_user = user.id_user');

        $this->db->where('kru.id_jadwal', $id_jadwal);

        $this->db->order_by('kru.id_kru', 'DESC');
        $this->db->limit($limit, $page-1);
        $query = $this->db->get();

        return $query->result_array();
    }


}